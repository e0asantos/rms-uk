package com.cemex.rms.dispatcher.views.factory
{
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	
	import flash.net.SharedObject;

	public class TimeDispatcher
	{
		private static var so:SharedObject=SharedObject.getLocal("rmsZoom","/");
		public static function setInitialZoom(value:Date):void{
			if(DispatcherViewMediator.currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				so.data["inicial"]=value;
				so.flush();
			}
		}
		
		public static function clearZoom():void{
			if(DispatcherViewMediator.currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				so.data["inicial"]=null;
				so.data["final"]=null;
				so.flush();
			}
		}
		
		public static function setFinalZoom(value:Date):void{
			if(DispatcherViewMediator.currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				so.data["final"]=value;
				so.flush();
			}
		}
		
		public static function getInitialZoom(value:Date):Date{
			//value parameter is the limit, if we are below the limit then return the limit itself
			if(DispatcherViewMediator.currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				if(so.data["inicial"]!=null){
					if(so.data["inicial"].time>=value.time){
						return so.data["inicial"];
					} else {
						//the date is older than expected
						setInitialZoom(value);
						return value;
					}
				} else {
					setInitialZoom(value);
					return value;
				}
			}
			return value;
		}
		
		public static function getFinalZoom(value:Date):Date{
			if(DispatcherViewMediator.currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				if(so.data["final"]!=null){
					if(so.data["final"].time<=value.time){
						return so.data["final"];
					} else {
						//the date is older than expected
						setFinalZoom(value);
						return value;
					}
				} else {
					setFinalZoom(value);
					return value;
				}	
			}
			return value;
		}
		
		public static function getCurrentZoom():String{
			if (so.data["inicial"]!=null){
				return so.data["inicial"].toString()+"/"+so.data["final"].toString();
			} else {
				return "NOT";
			}
		}
	}
}