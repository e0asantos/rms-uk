package com.cemex.rms.dispatcher.views.factory
{
	import com.cemex.rms.dispatcher.views.components.LabelExt;
	
	import flash.text.TextField;

	public class TextFieldFactory
	{
		public static function createTextField(valueText:String):TextField{
			var txtField:TextField = new TextField();
			txtField.text=valueText;
			return txtField;
		}
		public static function createLabel(valueText:String):LabelExt{
			var labl:LabelExt=new LabelExt();
			labl.text=valueText;
			
			return labl;
		}
	}
}