package com.cemex.rms.dispatcher.views.tree
{
	import com.cemex.rms.common.gantt.overlap.GanttOverlapTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	public class OrderLoadGanttOverlapTask extends GanttOverlapTask
	{
		public function OrderLoadGanttOverlapTask()
		{
			super();
		}
		
		public function get payload():OrderLoad{
			return data as OrderLoad;
		}
		public function set payload(_payload:OrderLoad):void{
			this.data = _payload;
		}
	}
}