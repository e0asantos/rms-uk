package com.cemex.rms.dispatcher.views.serviceAgentScreen.tree
{
	import com.cemex.rms.common.gantt.GanttResource;
	
	public class ServiceAgentScreenResource extends GanttResource
	{
		public var plant:String;
		public var orderLoad:String;
		public var orderNumber:String;
		public function ServiceAgentScreenResource()
		{
			super();
		}
		override public function get label():String{
			return super.label;
		}
		
	}
}