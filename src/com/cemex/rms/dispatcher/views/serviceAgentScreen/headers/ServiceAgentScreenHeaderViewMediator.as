package com.cemex.rms.dispatcher.views.serviceAgentScreen.headers
{
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.headers.ServiceAgentScreenHeaderView;
	
	import flash.events.Event;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class ServiceAgentScreenHeaderViewMediator extends Mediator
	{
		public function ServiceAgentScreenHeaderViewMediator()
		{
			super();
		}
		
		[Inject]
		public var view:ServiceAgentScreenHeaderView;
		
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		
		override public function onRegister():void {
			view.loadsPerVehicle.text=getLabel(OTRConstants.DISP_SEARCH);
			view.searchTextVehicle.addEventListener(Event.CHANGE,searchBtnClick);
			view.searchTextVehicle.text="";
		}
		
		public function searchBtnClick(evt:Event):void{
			DispatcherViewMediator.textToSearch=view.searchTextVehicle.text;
			var busqueda:SearchBtnClickViewEvent=new SearchBtnClickViewEvent();
			busqueda.searchString=view.searchTextVehicle.text;
			busqueda.refTextInput=view.searchTextVehicle;
			eventDispatcher.dispatchEvent(busqueda);
		}		
		
	}
}