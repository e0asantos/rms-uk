package com.cemex.rms.dispatcher.views.serviceAgentScreen.mediators
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleTask;
	import com.cemex.rms.dispatcher.views.mediators.GenericTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.renderers.ServiceAgentScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.serviceAgentScreen.tree.ServiceAgentScreenTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.filters.BitmapFilterQuality;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	
	import mx.core.UIComponent;
	
	public class ServiceAgentTaskRendererMediator extends GenericTaskRendererMediator
	{
		public function ServiceAgentTaskRendererMediator()
		{
			super();
		}
		
		[Inject]
		public var view:ServiceAgentScreenTaskRenderer;
		[Inject]
		public var services: IGanttServiceFactory;
		
		protected override function getView():UIComponent{
			return view;
		}
		override public function onRegister():void {
			super.onRegister();
			var task:ServiceAgentScreenTask = getGanttTask() as ServiceAgentScreenTask;
			if (task != null){
				task.setView(view);
			} 
			eventDispatcher.addEventListener(SearchBtnClickViewEvent.SEARCH_BTN_CLICK_EVENT,searchHandler);

		}
		
		public override function getViewName():String{
			view.invalidateDisplayList();
			view.invalidateProperties();
			view.validateNow();
			return TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID;
		}
		
		public override function getExtraInfoIfOverlap(task:GanttTask):String {
			var payload:OrderLoad = task.data as OrderLoad;
			return payload.orderNumber+":"+payload.itemNumber+":"+payload.loadNumber + "("+payload.loadingTime+")";
		}
		
		private function searchHandler(event:SearchBtnClickViewEvent):void{
			if((getGanttTask().data as OrderLoad).orderLoad.toLowerCase().indexOf(event.searchString.toLowerCase())>-1&&event.searchString!=""){
				view.filters = [_shadow, _searchHiglightFilter];
			}
			else{
				view.filters = [_shadow];
			}
		}
		
		//efecto borde azul para búsquedas
		private var _searchHiglightFilter:GlowFilter = new GlowFilter(0x23A1FF, 4, 6, 6, 6, BitmapFilterQuality.HIGH, false, false);
		//efecto sombra
		private var _shadow:DropShadowFilter = new DropShadowFilter(4,72,0,0.3,4,4);
				
	}
}