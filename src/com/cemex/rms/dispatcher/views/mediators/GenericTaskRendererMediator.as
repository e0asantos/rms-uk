﻿package com.cemex.rms.dispatcher.views.mediators
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.DispatcherConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	import com.google.maps.Constants;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.controls.Alert;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	import mx.events.ToolTipEvent;
	import mx.managers.ToolTipManager;
	import mx.utils.ObjectUtil;
	
	import ilog.gantt.TaskItem;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class GenericTaskRendererMediator extends Mediator
	{
		public function GenericTaskRendererMediator()
		{
			super();
			
		}
		public function getViewName():String{
			throw new Error("el metodo getViewName: " + this + "debe ser sobre escrito");
		}
		
		/*public function showChecked(action:String, task:GanttTask):Boolean{
			return true;
		}*/
	
		protected function getView():UIComponent{
			
			throw new Error("This method should be overriden");
		}
		public function getTooltiper():UIComponent{
			return getView();
		}
		override public function onRegister():void {
			getView().addEventListener(MouseEvent.DOUBLE_CLICK, popup,false,0,true);
			var currentViewName:*=getViewName();
			if(currentViewName!=TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID && currentViewName!=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID){
				getTooltiper().addEventListener(ToolTipEvent.TOOL_TIP_SHOW,tooltipShow,false,0,true);
				getTooltiper().addEventListener(ToolTipEvent.TOOL_TIP_START,tooltipStart,false,0,true);
				getTooltiper().toolTip = getTooltip();
			} 
		}
		public function tooltipShow(e:ToolTipEvent):void{
			var currentViewName:*=getViewName();
			var carg:Object=getGanttTask();
			if((carg.payload as OrderLoad).workAroundDestroy=="DELE" || (carg.payload as OrderLoad).loadStatus=="DELE"){
				return;
			}
			if (getTooltiper() != null && currentViewName!=TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID && currentViewName!=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID){
				e.toolTip.text = getTooltiper().toolTip ;
			} else {
				e.toolTip.text = "" ;
			}
		}
		public function getExtraInfoIfOverlap(task:GanttTask):String{
			var payload:OrderLoad = task.data  as OrderLoad;
			return payload.orderNumber+":"+payload.itemNumber ;
		}
		public function tooltipStart(e:ToolTipEvent):void{
			var currentViewName:*=getViewName();
			var carg:Object=getGanttTask();
			if((carg.payload as OrderLoad).workAroundDestroy=="DELE" || (carg.payload as OrderLoad).loadStatus=="DELE"){
				return;
			}
			if (getTooltiper() != null && currentViewName!=TVARVCPreffixes.PLANNING_SCREEN_VIEW_ID && currentViewName!=TVARVCPreffixes.SERVICE_AGENT_SCREEN_VIEW_ID){
				getTooltiper().toolTip = getTooltip();
			} else {
				getTooltiper().toolTip="";
			}
		}
		public function getTooltip():String{
			if (getGanttTask() != null){
				
				if (GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.LOAD_TOOLTIP,null)){
					return TooltipHelper.getLoadTooltip(getGanttTask());
					//return "hello world 1";
				}
				else if (GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.LOAD_TOOLTIP_PRODUCTION,null)){
					return TooltipHelper.getProdLoadTooltip(getGanttTask());
					//return "hello world 2";
				}
				else {
					return "";
				}
				
				
				
			}
			else {
				return "X";
			}
		}
		
		public static var taskActions:Menu;
		public function popup(event:MouseEvent):void {
			if (getGanttTask() != null 
				&& getGanttTask().step != GanttTask.STEP_GHOSTX){
				taskActions = PopupHelper.popupFromXML(event,"@label",GenericTaskRendererMediatorHelper.getTaskActions(getGanttTask()));
				taskActions.addEventListener(MenuEvent.ITEM_CLICK,GenericTaskRendererMediatorHelper.menuClick,false,0,true);
				/*taskActions.addEventListener(mou,menuClick);*/
				//makeRoundMenu(taskActions);
			} 
		}		
		/**
		 * Esta menuClick lo que hace es mandar a llamar la funcion que esta configurada en el XML 
		 * 
		 */
	
		/*
		Obtiene una referencia de este taskItem
		*/
		protected function getGanttTask():GanttTask {
			var ctask:UIComponent=getView();
			if (getView() != null
				&& getView()["data"] != null){
				return getView()["data"]["data"] as GanttTask;
			}
			return null;
		}
		
		protected function getTaskItem():TaskItem{
			if (getView() != null  && getView()["data"] != null) {
				return getView()["data"] as TaskItem;
			}
			return null;
		}	
		
	}
}