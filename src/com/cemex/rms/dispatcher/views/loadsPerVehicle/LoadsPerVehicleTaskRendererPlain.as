package com.cemex.rms.dispatcher.views.loadsPerVehicle
{
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.components.LabelExt;
	import com.cemex.rms.dispatcher.views.factory.TextFieldFactory;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.display.Shape;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.events.ResizeEvent;
	
	public class LoadsPerVehicleTaskRendererPlain extends Canvas
	{
		public static var globalWorkingLoads:Dictionary=new Dictionary(true);
		public function LoadsPerVehicleTaskRendererPlain()
		{
			super();
			this.horizontalScrollPolicy="off";
			this.verticalScrollPolicy="off";
			this.addEventListener(ResizeEvent.RESIZE,resizeHandler,false,0,true);
			this.addEventListener(FlexEvent.CREATION_COMPLETE,generateContent,false,0,true)
			
		}
		public function generateContent(evt:FlexEvent):void{
			this.removeEventListener(FlexEvent.CREATION_COMPLETE,generateContent);
			createTextFields();
			if(this.data.hasOwnProperty("data")){
				var txt:Object="hola";
			}
		}
		public var _colorBorder:uint=0x000000;
		public var _colorFill:uint=0xffffff;
		override public function set data(value:Object):void{
			if(value!=null){
				if(value.hasOwnProperty("data")){
					if(LoadsPerVehicleTaskRenderer.globalWorkingLoads[(value.data.payload as OrderLoad).orderNumber+":"+(value.data.payload as OrderLoad).itemNumber+":"+(value.data.payload as OrderLoad)._etenr]==null){
						LoadsPerVehicleTaskRenderer.globalWorkingLoads[(value.data.payload as OrderLoad).orderNumber+":"+(value.data.payload as OrderLoad).itemNumber+":"+(value.data.payload as OrderLoad)._etenr]=value.data.payload;
					}
					if(value.data.payload.pfillColor!=""){
						if(_colorBorder!=uint(value.data.payload.pfillColor) || _colorBorder!=uint(value.data.payload.pborderColor)){
							_colorFill=uint(value.data.payload.pfillColor);
							_colorBorder=uint(value.data.payload.pborderColor);
							redrawRectangle();
						}
					} else {
						if(_colorBorder!=uint(value.data.payload.fillColor) || _colorBorder!=uint(value.data.payload.borderColor)){
							_colorFill=uint(value.data.payload.fillColor);
							_colorBorder=uint(value.data.payload.borderColor);
							redrawRectangle();
						}
					}
					 
				}
			}
			super.data=value;
			setLoadText();
		}
		
		private function resizeHandler(evt:ResizeEvent):void{
			redrawRectangle();
		}
		
		private function redrawRectangle():void{
			this.graphics.clear();
			this.graphics.lineStyle(2,_colorBorder);
			this.graphics.beginFill(_colorFill); // choosing the colour for the fill, here it is red
			this.graphics.drawRoundRect(0, 0, this.width,this.height,5,5); // (x spacing, y spacing, width, height)
			this.graphics.endFill(); // not always needed but I like to put it in to end the fill
		}
		private function createTextFields():void{
			if(this.numChildren>1){
				this.removeAllChildren();
			}
			if(this.numChildren==0){
				addChild(TextFieldFactory.createLabel("one"));
			}
		}
		override public function set height(value:Number):void{
			if(this.data!=null){
				if((data.data.payload as OrderLoad)["workAroundDestroy"]=="DELE" || (data.data.payload as OrderLoad).loadStatus=="DELE"
					|| DispatcherViewMediator.nonVisilbleItems.getAvailableKeys().toArray().indexOf((data.data.payload as OrderLoad).orderNumber+":"+(data.data.payload as OrderLoad).itemNumber)!=-1){
					super.height=0;
					return;
				}
			}
			super.height=30;
		}
		override public function set y(value:Number):void{
			var adicion:Number=0;
			if(this.data!=null){
				if((data.data.payload as OrderLoad)["workAroundDestroy"]=="DELE" || (data.data.payload as OrderLoad).loadStatus=="DELE"){
					super.y=-900;
					return;
				}
				//if((this.data.data.payload as OrderLoad).orderLoad.indexOf("L")!=-1){
				adicion=Number((this.data.data.payload as OrderLoad).indexPosition)*30;
				//}
			}
			var val:Number=value+adicion;
			if(super.y!=value+adicion){
				super.y=value+adicion;
			}
		}
		private function setLoadText():void{
			createTextFields();
			var thisLoad:OrderLoad=data.data.payload as OrderLoad;
			(this.getChildAt(0) as LabelExt).text=thisLoad.valueAdded;
			(this.getChildAt(0) as LabelExt).text+="    "+thisLoad.constructionProduct;
			(this.getChildAt(0) as LabelExt).text+="    "+thisLoad.equipLabel;
			(this.getChildAt(0) as LabelExt).text+="    "+thisLoad.orderNumber;
			(this.getChildAt(0) as LabelExt).text+="-";
			(this.getChildAt(0) as LabelExt).text+=thisLoad.loadNumber;
			if(thisLoad.loadVolume!=thisLoad.confirmQty && thisLoad.confirmQty!=0 && (thisLoad.loadStatus=="ASSG" || thisLoad.loadStatus=="LDNG")){
				(this.getChildAt(0) as LabelExt).text+="  "+thisLoad.confirmQty+thisLoad.loadUom+" / "+thisLoad.loadVolume+thisLoad.loadUom
			} else {
				(this.getChildAt(0) as LabelExt).text+="  "+thisLoad.loadVolume+thisLoad.loadUom;
			}
			(this.getChildAt(0) as LabelExt).text+="   "+data.data.renegotiationLabel;
			if(GanttServiceReference.getParamString("DS_DISP_JS_NAME")=="1"){
				(this.getChildAt(0) as LabelExt).text+="   "+thisLoad.jobSiteName;
			}
			
		}
	}
}