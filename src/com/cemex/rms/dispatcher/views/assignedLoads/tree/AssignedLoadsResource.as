package com.cemex.rms.dispatcher.views.assignedLoads.tree
{
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;

	public dynamic class AssignedLoadsResource extends GanttResource
	{
		public function AssignedLoadsResource() {
			super();
			
		}
		public override function get label():String {
			var etiqueta:String = super.label;
			var additionalExtra:String="";
			if(super.labelField=="plant" && super.data.hasOwnProperty("name")){
				etiqueta=etiqueta+" "+super.data["name"];
			} else {
				if(GanttServiceReference.getParamString("DS_DISP_SO_CUST",this.data.plant)=="NUMCUS"){
					additionalExtra="\n"+super.data["soldToName"];
					
				}
			}

			return etiqueta+additionalExtra;
		}
	}
}