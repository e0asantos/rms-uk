package com.cemex.rms.dispatcher.views.assignedLoads.tree
{
	import com.cemex.rms.common.gantt.GanttTree;
	import com.cemex.rms.common.tree.NodeResource;
	import com.cemex.rms.common.tree.NodeTask;
	
	import mx.collections.ArrayCollection;

	/**
	 * Se utiliza otro Tree para que se puedan especificar diferentes Task y Resources y poder cutomizar
	 * las etiquetas y comportamientos
	 * 
	 * 
	 */
	public class AssignedLoadsTree extends GanttTree
	{
		public function AssignedLoadsTree(fields:ArrayCollection)
		{
			super(fields);
		}
		
		protected override function hasSameValues(target:Object,temp:Object):Boolean {
			var targetTask:AssignedLoadsTask = target as AssignedLoadsTask;
			var tempTask:AssignedLoadsTask = temp as AssignedLoadsTask;
			return targetTask != null && tempTask != null &&
				((""+targetTask.payload.itemNumber) == (""+tempTask.payload.itemNumber) )&&
				((""+targetTask.payload.orderNumber) == (""+tempTask.payload.orderNumber));
		}
		/*
		protected override function hasSameValues(target:Object,temp:Object):Boolean {
			var targetTask:AssignedLoadsTask = target as AssignedLoadsTask;
			var tempTask:AssignedLoadsTask = temp as AssignedLoadsTask;
			
			return  targetTask != null && tempTask != null &&
			(targetTask.payload.itemNumber == tempTask.payload.itemNumber) &&
				(targetTask.payload.orderNumber == tempTask.payload.orderNumber);
		}*/
		protected override function newNode():NodeResource{
			return new AssignedLoadsResource();
		}
		protected override function newTask():NodeTask {
			return new AssignedLoadsTask();
		}
		
		public override function timeChangedTask(time:Date,nodeTask:NodeTask):Boolean{
			var task:AssignedLoadsTask = nodeTask as AssignedLoadsTask; 
			task.initExtraValues();
			getTasksTree().itemUpdated(task);
			return true;
		}
		
		
		override public function deleteOldNode(oldNode:NodeResource,node:NodeResource,field:String):Boolean{
			/*
			switch(field){
				//case "orderNumber":
				case "equipNumber":
					return oldNode.data[field] != node.data[field];
			}*/
			return false;
		}
	}
}