package com.cemex.rms.dispatcher.views.assignedLoads.tree
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.ColorHelper;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ColorSetting;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsTaskRenderer;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.tree.OrderLoadGanttTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.sampler.NewObjectSample;
	
	import mx.binding.utils.BindingUtils;
	import mx.controls.Alert;
	import mx.events.FlexEvent;

	public class AssignedLoadsTask extends OrderLoadGanttTask
	{
		public function AssignedLoadsTask()
		{
			super();
		}
		public override function get startTime():Date{
			if (payload == null){
				return null;
			}/*
			if (payload.startTime == null ){
				payload.startTime = getStartTime(payload);
			}*/
			return payload.startTime;
		}
		public override function set startTime(_startTime:Date):void{
			if (data != null){
				data.startTime = _startTime;
			}
		}
		public override function get endTime():Date {
			if (payload == null){
				return null;
			}
			return getEndTime(startTime,payload);
		}
		public override function set endTime(_endTime:Date):void{
			if (data != null){
				data.endTime = _endTime;
			}
		}
		
		
		[Bindable]
		public var colorsConfiguration:Array=[];
		[Bindable]
		public var fillColors:Array=[];
		[Bindable]
		public var fillAlphas:Array=[];
		[Bindable]
		public var gradientRatio:Array=[];
		[Bindable]
		public var offsetX:Array=[];
		
		
		public function getStartTime(data:Object):Date{
			var payload:OrderLoad = data as OrderLoad;
			return FlexDateHelper.parseTime(payload.loadingDate,payload.loadingTime);
			//return FlexDateHelper.parseTime(FlexDateHelper.getDateObject(payload.schedLineDate),payload.loadingTime);
		}
		public function getEndTime(startTime:Date,data:Object):Date{
			var payload:OrderLoad = data as OrderLoad;
			return FlexDateHelper.addSeconds(startTime,payload.cycleTimeMillis);
		}
		
		
		private var logger:ILogger = LoggerFactory.getLogger("AssignedLoads");
		
		
		
		
		public override function initExtraValues():void {
			super.initExtraValues();
			
			
			if (payload == null ){
				return ;
			}
			payload.startTime = getStartTime(payload);	
			if (StatusHelper.isRenegotiated(payload)){
				renegotiationLabel = GanttServiceReference.getLabel(OTRConstants.RENEGOTIATION_LABEL);
			}
		
			
			//var currColor:ColorSetting = ColorHelper.getColor(payload,TVARVCPreffixes.LOAD_SCOPE);
			color = 0xFFFF00;
			border =  -1;
			thickness =  -1;
			delivTimeLabel="";
			dynamicColors();
			delivTimeLabel += FlexDateHelper.getDateString(startTime,"HH:NN") + "->" + FlexDateHelper.getDateString(endTime,"HH:NN");
			//invalidateAlgo(0);
		}
		
		public function getCoordinate(date:Date):Number{
			return DispatcherViewMediator.getCoordinate(date);
		}
		public function dynamicColors():void{
			var startTimeN:Number=null;
			if(startTime==null){
				startTimeN= 0;
			} else {
				startTimeN= startTime.getTime();
			}
			
			var endTimeN:Number =null; 
			if(endTime==null){
				endTimeN=10;
			} else {
				endTimeN=endTime.getTime();
			} 
			
			
			//var edgeDate:Date = new Date();//getPlantVisibilityLineDate();
			var edgeDate:Date=TimeZoneHelper.getServer();
			
			var edgeTime:Number = edgeDate.getTime();//now.getTime();
			if (edgeTime < startTimeN){
				colorsConfiguration = [1];	
				fillColors = [getRightColor()];	
				fillAlphas = [1];	
				//gradientRatio = [0];	
				offsetX = [0];	
			}
			else if (edgeTime < endTimeN){		
				colorsConfiguration =  [2];
				fillColors = [getLeftColor(),getLeftColor()];//[getLeftColor(),getRightColor()];
				fillAlphas = [1,1];//,0.50];
				gradientRatio = [0,1];
				
				var per:Number = 1;
				if(_view != null){
					per = _view.width / _view.box.width;
					//Alert.show(_view.width+" / "+_view.box.width);
				}
				
				var startCoord:Number = getCoordinate(payload.startTime);
				var nowCoord:Number = getCoordinate(edgeDate);
				
				if(_view!=null){
					_view.box.width=nowCoord - startCoord;
				}
				
				//var taskTime:Number = endTime - startTime; 
				//var middle:Number = nowTime - startTime;
				//var temp:Number = (middle / taskTime) * 100;
				//offsetX = [temp*per,temp*per];
				var middleCoord:Number = nowCoord - startCoord;
				if (_view){
					middleCoord = nowCoord - _view.x;
					//middleCoord = middleCoord - (middleCoord *.05);
					//delivTimeLabel= "(mc:"+middleCoord+"="+nowCoord +"-"+ startCoord+"["+_view.x+"])";
					offsetX = [ middleCoord,middleCoord];
					
					
					//offsetX = [ _view.width/2,_view.width/2];
				}
				else {
					offsetX = [ 0,0];
				}
			}
			else {
				if(_view!=null){
					_view.box.width=_view.width;
				}
				colorsConfiguration = [1];
				fillColors = [getLeftColor()];
				fillAlphas = [1];	
				gradientRatio = [255];
				offsetX = [100];
			}
		}
		
		public static function getLeftColor():Number{
			return 0xFF0000;
		}
		public static function getRightColor():Number{
			return 0xCCCCCC;
		}
		/*
		public function getPlantVisibilityLineTimeMilliseconds():Number{
			
			var now:Date = new Date();
			return now.getTime() + GanttServiceReference.getParamNumber(ParamtersConstants.PLANT_VIS_AREA_L)* 60 * 1000;
			
		}
		*/
		
		public function getPlantVisibilityLineDate():Date{
			
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			return FlexDateHelper.addMinutes(now,GanttServiceReference.getParamNumber(ParamtersConstants.PLANT_VIS_AREA_L));
			//return now.getTime() + * 60 * 1000;
			//return FlexDateHelper.addMinutes(now,60);
			
		}
		
		public var delivTimeLabel:String;
		
		
		
		private var _view:AssignedLoadsTaskRenderer;
		
		public function setView(_view:AssignedLoadsTaskRenderer):void{
			this._view = _view;
			initExtraValues();
			//BindingUtils.bindSetter(invalidateAlgo,this._view,["width"]);
			//BindingUtils.bindSetter(invalidateAlgo,this._view,["x"]);
		}
		
		public function invalidateAlgo(e:Number):void{
			if (_view  !=  null){
			
				_view.dispatchEvent(new FlexEvent(FlexEvent.DATA_CHANGE));
				_view.box.dispatchEvent(new FlexEvent(FlexEvent.DATA_CHANGE));
				_view.box.setStyle("offsetX",offsetX);
				_view.box.invalidateDisplayList();
				_view.box.invalidateProperties();
				_view.box.invalidateSize();
			}
		}
		
		
		
	
	}
}