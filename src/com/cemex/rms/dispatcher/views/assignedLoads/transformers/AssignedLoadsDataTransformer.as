package com.cemex.rms.dispatcher.views.assignedLoads.transformers
{
	
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.transformer.FieldColumn;
	import com.cemex.rms.common.transformer.GenericDataTransformer;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsResourceRenderer;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsTaskRenderer;
	import com.cemex.rms.dispatcher.views.assignedLoads.headers.AssignedLoadsHeader;
	import com.cemex.rms.dispatcher.views.assignedLoads.tree.AssignedLoadsResource;
	import com.cemex.rms.dispatcher.views.assignedLoads.tree.AssignedLoadsTree;
	
	import ilog.gantt.GanttDataGrid;
	
	import mx.collections.ArrayCollection;
	import mx.core.ClassFactory;
	import mx.core.IFactory;

	
	/**
	 * Esta clase transforma los datos para que en el arbol se vean Primero las plantas y despues,como hijos las ordenes 
	 */
	public class AssignedLoadsDataTransformer extends GenericDataTransformer
	{
		public function AssignedLoadsDataTransformer() {
			super();
			
			setFields(new ArrayCollection(["plant","orderLoad"])
				);//,AssignedLoadsTooltipHelper.getResourceData());
			
			setFieldColumns(new ArrayCollection([
				new FieldColumn("label",null,"Tree",160,null,AssignedLoadsHeader),

				new FieldColumn("label",null,"",25,AssignedLoadsResourceRenderer)
			]));
		}
		
		public override function getTaskRenderer():IFactory{
			return new ClassFactory(AssignedLoadsTaskRenderer);
		}
		
		public override function getResourceRenderer():IFactory{
			return null;
		}
	
		public override function getViewID():String{
			return TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID;
		}
		public override function setFields(fields:ArrayCollection):void{
			this.fields =  fields;
			tree = new AssignedLoadsTree(fields);
		}
		
		public override function setTreePlusMinus(dataGrid:GanttDataGrid):void{
			
			dataGrid.setStyle("disclosureOpenIcon", minus);
			dataGrid.setStyle("disclosureClosedIcon", plus);
			
		}
		
		[Embed(source="/assets/plus.gif")]
		public var plus:Class;
		
		[Embed(source="/assets/minus.gif")]
		public var minus:Class; 
		
		public override function setColumnsDataGridCustomization(dataGrid:GanttDataGrid):void{
			tree.currentDataGrid=dataGrid;
			dataGrid.iconFunction = iconFunction;
			dataGrid.styleFunction = null;
		}
		[Embed(source="/assets/s_b_plnt.gif")]            
		[Bindable]             
		private var plant:Class;
		
		[Embed(source="/assets/s_s_wspo.gif")]            
		[Bindable]             
		private var order:Class;
		
		[Embed(source="/assets/s_b_plnt_distr.gif")]            
		[Bindable]             
		private var plant_distribuida:Class;
		
		[Embed(source="/assets/s_b_plnt_centr.gif")]            
		[Bindable]             
		private var plant_centralizada:Class;
		
		[Embed(source="/assets/s_b_plnt_auton.gif")]            
		[Bindable]             
		private var plant_autonoma:Class;
		
		[Embed(source="/assets/s_b_plnt_centr_stop.png")]            
		[Bindable]             
		private var plant_centralizada_stop:Class;
		
		[Embed(source="/assets/s_b_plnt_distr_closed2.png")]            
		[Bindable]             
		private var plant_distribuida_stop:Class;
		
		private function iconFunction(resource:GanttResource):Class{
		
			
			if (resource.getLabelField() == "orderLoad"){
				return order;
			}
			else if (resource.getLabelField() == "plant"){
				var plantId:String = (""+resource.data["plantId"]);
				var plantType:String = GanttServiceReference.getPlantType(plantId);
				var currPlant:Object;
				var plantFull:ArrayCollection=GanttServiceReference.getPlants();
				for(var vi:int=0;vi<plantFull.length;vi++){
					if(plantId==plantFull.getItemAt(vi).plantId){
						currPlant=plantFull.getItemAt(vi);
					}
				}
				//Alert.show(plantId +"-" + plantType);
				if (plantType == "01" && currPlant.status=="E0001"){
					return plant_centralizada;
				} else if(plantType == "01" && currPlant.status=="E0002"){
					return plant_distribuida_stop;
				} else if(plantType=="01" && currPlant.status=="E0003"){
					return plant_centralizada_stop
				} else if (plantType == "02"/*GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low*/){
				//if (plantType == "02"){
					return plant_distribuida;	
				}
					else if (plantType == "01"/*GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low*/){
				//else if (plantType == "01"){
					return plant_centralizada;
				}
					else if (plantType == "03"/*GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low*/){
				//else if (plantType == "03"){
					return plant_autonoma;
				}
			}
			return null;
		}
		
		

	}
}