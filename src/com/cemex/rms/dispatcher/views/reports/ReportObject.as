package com.cemex.rms.dispatcher.views.reports
{
	import mx.collections.ArrayCollection;

	public class ReportObject 
	{
		public function ReportObject()
		{
		}
		
		
		public var singleton:Boolean = false;
		private var _reportsConfig:ArrayCollection;
		
		public function getReportsConfig():ArrayCollection{
			return _reportsConfig;
		}
		public function setReportsConfig(_reportsConfig:ArrayCollection):void{
			this._reportsConfig = _reportsConfig;
		}
		
		
		public function addData(data:Object):void {
			
			if (_reportsConfig != null){
				for (var i:int = 0 ; i <_reportsConfig.length ; i++ ){
					var config:ReportConfig = _reportsConfig.getItemAt(i) as ReportConfig;
					if (config != null){
						//config.addData(data,this);
					}
				}
			}
		}
		
		public function delData(data:Object):void {
			if (_reportsConfig != null){
				for (var i:int = 0 ; i <_reportsConfig.length ; i++ ){
					var config:ReportConfig = _reportsConfig.getItemAt(i) as ReportConfig;
					if (config != null){
						config.delData(data,this);
					}
				}
			}
			
		}
	}
}