package com.cemex.rms.dispatcher.views.loadsPerOrder
{
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.ColorHelper;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ColorSetting;
	import com.cemex.rms.dispatcher.views.components.LabelExt;
	import com.cemex.rms.dispatcher.views.factory.TextFieldFactory;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.containers.Canvas;
	import mx.events.FlexEvent;
	import mx.events.ResizeEvent;
	
	public class LoadPerOrderTaskRenderer extends Canvas
	{
		public var _colorBorder:uint=0x000000;
		public var _colorFill:uint=0xffffff;
		public function LoadPerOrderTaskRenderer()
		{
			super();
			this.horizontalScrollPolicy="off";
			this.verticalScrollPolicy="off";
			this.addEventListener(ResizeEvent.RESIZE,resizeHandler,false,0,true);
			this.addEventListener(FlexEvent.CREATION_COMPLETE,generateContent,false,0,true)
		}
		public function generateContent(evt:FlexEvent):void{
			this.removeEventListener(FlexEvent.CREATION_COMPLETE,generateContent);
			createTextFields();
			if(this.data.hasOwnProperty("data")){
				var txt:Object="";
			}
		}
		private function resizeHandler(evt:ResizeEvent):void{
			redrawRectangle();
		}
		
		
		
		private function redrawRectangle():void{
			this.graphics.clear();
			this.graphics.lineStyle(2,_colorBorder);
			this.graphics.beginFill(_colorFill); // choosing the colour for the fill, here it is red
			this.graphics.drawRoundRect(0, 0, this.width,this.height,5,5); // (x spacing, y spacing, width, height)
			this.graphics.endFill(); // not always needed but I like to put it in to end the fill
		}
		private function createTextFields():void{
			if(this.numChildren>1){
				this.removeAllChildren();
			}
			if(this.numChildren==0){
				addChild(TextFieldFactory.createLabel("one"));
			}
		}
		override public function set x(value:Number):void{
			if(this.data!=null){
				if((data.data.payload as OrderLoad)["workAroundDestroy"]=="DELE" || (data.data.payload as OrderLoad).loadStatus=="DELE"
					|| DispatcherViewMediator.nonVisilbleItems.getAvailableKeys().toArray().indexOf((data.data.payload as OrderLoad).orderNumber+":"+(data.data.payload as OrderLoad).itemNumber)!=-1
					|| itemDestroyable(data)){
					super.x=-900;
					return;
				}
			}
			super.x=value;
		}
		override public function set height(value:Number):void{
			if(this.data!=null){
				if((data.data.payload as OrderLoad)["workAroundDestroy"]=="DELE" || (data.data.payload as OrderLoad).loadStatus=="DELE"
					|| DispatcherViewMediator.nonVisilbleItems.getAvailableKeys().toArray().indexOf((data.data.payload as OrderLoad).orderNumber+":"+(data.data.payload as OrderLoad).itemNumber)!=-1
					|| itemDestroyable(data)){
					super.height=0;
					return;
				}
			}
			if(super.height<50){
				super.height=value;
			} else{
				super.height=50;
			}
		}
		override public function set y(value:Number):void{
			var adicion:Number=0;
			if(this.data!=null){
				//if((this.data.data.payload as OrderLoad).orderLoad.indexOf("L")!=-1){
				adicion=Number((this.data.data.payload as OrderLoad).indexPosition)*50;
				//}
			}
			super.y=value+adicion;
		}
		private function itemDestroyable(value:Object):Boolean{
			var notAllowed:Object=DispatcherViewMediator.visibleLoadItemsPlain;
			if((value.data.payload as OrderLoad).loadStatus=="ASSG"
				|| (value.data.payload as OrderLoad).loadStatus=="DELE"
				|| (value.data.payload as OrderLoad).loadStatus=="CMPL"/*  || (value.data.payload as OrderLoad).loadingDate.date!=DispatcherIslandImpl.fechaBase.date */
				|| (data.data.payload as OrderLoad).loadStatus=="TJST"){
					return true;
			}
			return false;
		}
		override public function set data(value:Object):void{
			super.data=value;
			if(value!=null){
				if(value.hasOwnProperty("data")){
					LoadsPerVehicleTaskRenderer.globalWorkingLoads[(value.data.payload as OrderLoad).orderNumber+":"+(value.data.payload as OrderLoad).itemNumber]=value.data.payload;
						//cpWidth
						
						///pump
						//normal load
						if(itemDestroyable(value)){
							this.x=-900;
							this.height=0;
							return;
						}
						setCPText();
				}
			}
		}
		
		public function setCPText():void{
			createTextFields();
			var thisLoad:OrderLoad=data.data.payload as OrderLoad;
			
			
			
			if(thisLoad.constructionProduct!=null){
				if(thisLoad.constructionProduct!=""){
					(this.getChildAt(0) as LabelExt).text=thisLoad.constructionProduct;
				} else if(thisLoad.loadNumber!="") {
					(this.getChildAt(0) as LabelExt).text=thisLoad.loadNumber;
				} else {
					(this.getChildAt(0) as LabelExt).text=GanttServiceReference.getLabel(OTRConstants.PUMPING_SERVICE);
				}
			}
			(this.getChildAt(0) as LabelExt).text+="  "+thisLoad.deliveryTime;
			if(GanttServiceReference.getParamString("DS_DISP_JS_NAME")=="1"){
				(this.getChildAt(0) as LabelExt).text+="  "+thisLoad.jobSiteName;
			}
			(this.getChildAt(0) as LabelExt).text+="\n";
			var conf:ColorSetting=ColorHelper.getColorFrom(TVARVCConstants.COLOR_DELAYED,TVARVCPreffixes.LOAD_SCOPE);
			if(data.data.hasOwnProperty("delayTimeLabel")){
				var tiempo:Number=Number(String(data.data.delayTimeLabel).substr(1));
				var colorSetting:ColorSetting=ColorHelper.getColorFrom(TVARVCConstants.COLOR_VISIBLE_AT_PLANT,TVARVCPreffixes.LOAD_SCOPE);
				(this.getChildAt(0) as LabelExt).text+="D"+String(Math.floor(tiempo/60))+":"+String(Math.floor(tiempo%60));
				if(tiempo>0){
					if(_colorBorder!=conf.borderColor || _colorFill!=conf.fillColor){
						_colorFill=uint(conf.fillColor);
						_colorBorder=uint(conf.borderColor);
						redrawRectangle();
					}
					 
				} else {
					if(_colorBorder!=uint(thisLoad.borderColor) || _colorFill!=uint(thisLoad.fillColor)){
						_colorFill=uint(thisLoad.fillColor);
						_colorBorder=uint(thisLoad.borderColor);
						redrawRectangle();
					}
					
				}
			}
			
			conf=null;
			if(thisLoad.loadVolume!=thisLoad.confirmQty && thisLoad.confirmQty!=0 && thisLoad.loadStatus=="ASSG"){
				(this.getChildAt(0) as LabelExt).text+="  "+thisLoad.confirmQty+thisLoad.loadUom+" / "+thisLoad.loadVolume+thisLoad.loadUom
			} else {
				(this.getChildAt(0) as LabelExt).text+="  "+thisLoad.loadVolume+thisLoad.loadUom;
			}
			(this.getChildAt(0) as LabelExt).text+="\n";
			if(thisLoad.equipStatus!=""){
				(this.getChildAt(0) as LabelExt).text+=thisLoad.equipLabel;
			} else {
				(this.getChildAt(0) as LabelExt).text+="-"
			}
			if(DispatcherViewMediator.currentInstance.isPerHour){
				(this.getChildAt(0) as LabelExt).text+="  "+thisLoad.plant;
			}
		}
		
	}
}