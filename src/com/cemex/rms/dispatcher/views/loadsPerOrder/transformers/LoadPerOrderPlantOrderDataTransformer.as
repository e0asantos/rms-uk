package com.cemex.rms.dispatcher.views.loadsPerOrder.transformers
{
	
	import com.cemex.rms.common.transformer.FieldColumn;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderPlantOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.headers.LoadsPerOrderPlantOrderHeader;
	
	import mx.collections.ArrayCollection;
	import mx.core.ClassFactory;
	import mx.core.IFactory;

	
	/**
	 * Esta clase transforma los datos para que en el arbol se vean Primero las plantas y despues,como hijos las ordenes 
	 */
	public class LoadPerOrderPlantOrderDataTransformer extends LoadsPerOrderAbstractDataTransformer
	{
		public function LoadPerOrderPlantOrderDataTransformer() {
			super();
			
			setFields(new ArrayCollection(["plant","orderNumber"])
			);//,LoadPerOrderTooltipHelper.getResourceData());
			
			setFieldColumns(new ArrayCollection([
				new FieldColumn("label",null,"",160,null,LoadsPerOrderPlantOrderHeader),
				new FieldColumn("label",null,"",25,LoadPerOrderPlantOrderResourceRenderer)
				//new FieldColumn("plant","Plant",70),
				
			]));
			
		}
	
		
		public override function getTaskRenderer():IFactory{
			return new ClassFactory(LoadPerOrderTaskRenderer);
		}
		
		//public override function getSortFunction(field:String):Function {
		//	return sorter;
		//}
		
		
		public function loadPerOrderByScheduleField():void {
			//super();
			
			setFields(new ArrayCollection(["orderNumber"])
			);//,LoadPerOrderTooltipHelper.getResourceData());
			
			
			
		}
		
		public function loadPerOrderByPlantField():void {
			//super();
			
			setFields(new ArrayCollection(["plant","orderNumber"])
			);//,LoadPerOrderTooltipHelper.getResourceData());
			
			
			
		}
	
		

	}
}