package com.cemex.rms.dispatcher.views.components
{
	import flash.display.DisplayObject;
	import flash.text.TextFieldAutoSize;
	
	import mx.controls.Label;
	import mx.core.UITextField;
	
	public class LabelExt extends Label
	{
		public function LabelExt()
		{
			super();
		}
		public function getTextField():* {
			return textField
		}
		override protected function createChildren() : void
		{
			// Create a UITextField to display the label.
			if (!textField)
			{
				textField = new UITextField();
				textField.styleName = this;
				addChild(DisplayObject(textField));
			}
			super.createChildren();
			textField.multiline = true;
			textField.wordWrap = true;
			textField.autoSize = TextFieldAutoSize.LEFT;
		}
	}
}