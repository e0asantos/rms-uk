package com.cemex.rms.dispatcher.events
{
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	import flash.events.Event;
	/**
	 * Este evento le avisa a todos los mediadores que lo requieran que el tiempo cambio
	 * para que cambien su vista
	 * 
	 * @author japerezh
	 * 
	 */	
	public class InvalidateBindingsEvent extends Event
	{
		public static const INVALIDATE_BINDINGS:String ="invalidateBindings";
		
		
		public function InvalidateBindingsEvent(bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(INVALIDATE_BINDINGS, bubbles, cancelable);
		
		}
	}
}