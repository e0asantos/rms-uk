package com.cemex.rms.dispatcher.events
{
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.events.Event;
	
	import mx.controls.TextInput;

	/**
	 * Este evento le avisa a todos los mediadores que lo requieran que el tiempo cambio
	 * para que cambien su vista
	 * 
	 * @author japerezh
	 * 
	 */	
	public class SearchBtnClickViewEvent extends Event
	{
		public var searchString:String;
		public var refTextInput:TextInput;
		
		public static const SEARCH_BTN_CLICK_EVENT:String="SearchBtnClickViewEvent;"
		
		public function SearchBtnClickViewEvent(bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(SEARCH_BTN_CLICK_EVENT, bubbles, cancelable);
		}
	}
}