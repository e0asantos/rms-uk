package com.cemex.rms.dispatcher.events
{
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.events.Event;
	/**
	 * Esta clase nos Avisa que en la Vista hubo un Movimiento, y este movimiento se debe de: 
	 * - Hacer como Ghost
	 * - Hacer la solicitud a los servicios
	 * 
	 * @author japerezh
	 * 
	 */	
	public class GanttTaskMovementEvent extends Event
	{
		public static const GANTT_TASK_MOVE:String ="ganttTaskMove";
		
		public var origin:OrderLoad;
		public var destiny:OrderLoad;
		
		public function GanttTaskMovementEvent(origin:OrderLoad,destiny:OrderLoad, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(GANTT_TASK_MOVE, bubbles, cancelable);
			this.origin = origin;
			this.destiny = destiny;
		}
	}
}