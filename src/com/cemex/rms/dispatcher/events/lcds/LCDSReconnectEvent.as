package com.cemex.rms.dispatcher.events.lcds
{
	import com.cemex.rms.common.gantt.GanttTask;
	
	import flash.events.Event;

	public class LCDSReconnectEvent extends Event
	{
		public static const LCDS_RECONNECT_REQUEST:String="lcdsReconnectRequest";

		public var task:GanttTask;
		
		public function LCDSReconnectEvent( bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(LCDS_RECONNECT_REQUEST, bubbles, cancelable);
		}
	}
}