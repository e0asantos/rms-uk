package com.cemex.rms.dispatcher.events
{
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	import flash.events.Event;
	/**
	 * Este evento le avisa a todos los mediadores que lo requieran que el tiempo cambio
	 * para que cambien su vista
	 * 
	 * @author japerezh
	 * 
	 */	
	public class TimeChangedEvent extends Event
	{
		public static const TIME_CHANGED:String ="timeChangedNotice";
		
		public var currentTime:Date;
		
		public function TimeChangedEvent(currentTime:Date, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(TIME_CHANGED, bubbles, cancelable);
			this.currentTime = currentTime;
		}
	}
}