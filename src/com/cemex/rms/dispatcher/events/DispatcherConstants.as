package com.cemex.rms.dispatcher.events
{
	public class DispatcherConstants
	{
		public function DispatcherConstants()
		{
		}
		public static const LOAD_SCOPE:String = "LOAD";
		public static const ORDER_SCOPE:String = "ORDER";
		
		
		public static const EVENT_TYPE_MENU:String = "MENU";
		public static const EVENT_TYPE_DRAG_DROP:String = "DRAG_DROP";
		
		
	}
}