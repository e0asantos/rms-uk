package com.cemex.rms.dispatcher.events
{
	import flash.events.Event;
	
	import mx.controls.TextInput;
	
	public class SearchBtnVehicleEvent extends Event
	{
		public var searchString:String;
		public var refTextInput:TextInput;
		
		public static const SEARCH_BTN_VEHICLE_CLICK_EVENT:String="SearchBtnVehicleClickViewEvent;"
		public function SearchBtnVehicleEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}