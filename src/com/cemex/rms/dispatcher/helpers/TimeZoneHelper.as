package com.cemex.rms.dispatcher.helpers
{
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	
	import mx.formatters.DateFormatter;

	public class TimeZoneHelper
	{
		public function TimeZoneHelper()
		{
		}
		public static var hourOffSetTime:Number=0;
		public static var initialHour:Date=null;
		public static var lockHour:Boolean=false;
		public static var lastKnownHour:Date=null;
		public static var lastMinute:Number=0;
		public static var lastServerMinute:Number=-1;
		
		public static function buildTime(currentDate:Date):String{
			/*var mdate:String=(currentDate.fullYear.toString());
			var mes:String=(currentDate.month+1).toString();
			if(Number(mes)<10){
				mes="0"+mes;
			}
			var dia:String=(currentDate.getDate()).toString();
			if(Number(dia)<10){
				dia="0"+dia;
			}
			var hora:String=*/
			
			var dateFormatter : DateFormatter = new DateFormatter();
			dateFormatter.formatString = "YYYYMMDDHHNNSS";
			
			return dateFormatter.format(currentDate);
		}
		public static function buildDifferenceTime():String{
			var currentDate:Date=new Date();
			currentDate.setTime(currentDate+DispatcherIslandImpl.serverConst);
			var dateFormatter : DateFormatter = new DateFormatter();
			dateFormatter.formatString = "YYYYMMDDHHNNSS";
			return dateFormatter.format(currentDate);
		}
		public static function buildDateDifference():Date{
			var currentDate:Date=new Date();
			currentDate.setTime(currentDate.getTime()+DispatcherIslandImpl.serverConst);
			var milis:Number=DispatcherIslandImpl.serverConst;
			return currentDate;
		}
		public static function unBuildTime(currentDateString:String):Date{
			var buildDate:Date=new Date();
			buildDate.fullYear=Number(currentDateString.substr(0,4));
			buildDate.month=Number(currentDateString.substr(4,2))-1;
			buildDate.date=Number(currentDateString.substr(6,2));
			buildDate.hours=Number(currentDateString.substr(8,2));
			buildDate.minutes=Number(currentDateString.substr(10,2));
			buildDate.seconds=Number(currentDateString.substr(12,2));
			return buildDate;
		}
		
		public static function getServer():Date{
			var hora:Date=DispatcherIslandImpl.fechaBase;
			if(TimeZoneHelper.lastKnownHour!=null && hora!=null && !isNaN(hora.date)){
				if(TimeZoneHelper.lastServerMinute==-1){
					TimeZoneHelper.lastServerMinute=hora.minutes;
				} 
				TimeZoneHelper.lastKnownHour=hora;
			}
			if(TimeZoneHelper.initialHour==null){
				TimeZoneHelper.initialHour=new Date();
				
			}
			if(hora==null || isNaN(hora.date)){
				if(TimeZoneHelper.lastKnownHour==null){
					hora=new Date();
				} else {
					hora=TimeZoneHelper.lastKnownHour;
				}
			} 
			/*if(hora.hours!=(new Date()).hours){
				TimeZoneHelper.hourOffSetTime=hora.hours-(new Date()).hours;
			}*/
			var dummy:Date=new Date();
			if(dummy.minutes!=TimeZoneHelper.lastMinute){
				TimeZoneHelper.lastMinute=dummy.minutes;
				if(TimeZoneHelper.lastServerMinute!=-1){
					if(TimeZoneHelper.lastServerMinute!=hora.minutes){
						TimeZoneHelper.lastServerMinute=hora.minutes;
					} else {
						TimeZoneHelper.lastServerMinute=TimeZoneHelper.lastServerMinute+1;	
					}
					
					if(TimeZoneHelper.lastServerMinute==60){
						TimeZoneHelper.lastServerMinute=0;
						hora.hours=hora.hours+1;
					}
				} else if(TimeZoneHelper.lastServerMinute==-1){
					
					if(DispatcherIslandImpl.fechaBase!=null){
						TimeZoneHelper.lastServerMinute=DispatcherIslandImpl.fechaBase.minutes;	
					}
				}
			}
			/*if(dummy.hours!=TimeZoneHelper.initialHour.hours){
				TimeZoneHelper.initialHour=new Date();
				hora.hours=hora.hours+1;
			}*/
			hora.minutes=TimeZoneHelper.lastServerMinute;
			hora.milliseconds=dummy.milliseconds;
			hora.seconds=dummy.seconds;
			//tenemos retraso de 1 minuto
			//hora.minutes=hora.minutes+1;
			TimeZoneHelper.lastKnownHour=new Date();
			TimeZoneHelper.lastKnownHour.hours=hora.hours;
			TimeZoneHelper.lastKnownHour.minutes=hora.minutes;
			//hora.date=(new Date()).date;
			return hora;
		}
	}
}