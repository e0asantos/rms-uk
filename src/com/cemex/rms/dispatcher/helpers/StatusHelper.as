package com.cemex.rms.dispatcher.helpers
{
	import com.cemex.rms.common.filters.FilterCondition;
	import com.cemex.rms.common.filters.FilterHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	
	public class StatusHelper extends GanttServiceReference
	{
		public function StatusHelper()
		{
			// CPTYPE = ZTNR
			// CONCRETE = ZTC1
			//ADITION = ZADR | ZTXR
			// PUMP = 
			/*
			
			
			
			
			
			ZTAT   Concrete (COMERCIAL)   NO SE DESPLIEGA, sin embargo se puede utilizar en ciertas funcionalidades para actualizar o leer algún campo.
			XXXXXXXXXXXX
			
			ZTC1   Concrete Load Split .  SE DESPLIEGA Y SE ACTUALIZA. Solo va a tener relación con ZADR
			Z_CXFM_CSDSLSMX_1005_CONCRETE,S
			
			ZTX0   Pump Services          antes ZTSR? (antes ZTAD).   SE DESPLIEGA DE ACUERDO A PARAMETRO. Se actualiza y valida en ciertas funcionalidades
			Z_CXFM_CSDSLSMX_1005_PUMP,S
			
			ZTXR   Additional Charges    NO SE DESPLIEGA NI SE ACTUALIZA, NO DEBEN PROCESARSE EN LA SABANA DE DESPACHO
			Z_CXFM_CSDSLSMX_1005_ADDITION,S
			
			ZADR  Additives (Value Added).  NO se despliegan por si solas, pero se leen para desplegar tooltip y se actualizan
			XXXXXXXXXXXX
			
			ZTNR   Construction Product.   SE DESPLIEGA DE ACUERDO A PARAMETRO. Se actualiza y valida en ciertas funcionalidades. 
			Z_CXFM_CSDSLSMX_1005_CPTYPE,S
			
			*/
		}
		
		private static var statuses:DictionaryMap =  new DictionaryMap(); 
		public static function initFilters():void{
			
			statuses.put(TVARVCPreffixes.CANCELLED, getFilter(TVARVCPreffixes.CANCELLED));
			statuses.put(TVARVCPreffixes.CONFIRMED, getFilter(TVARVCPreffixes.CONFIRMED));
			statuses.put(TVARVCPreffixes.DELAYED, getFilter(TVARVCPreffixes.DELAYED));
			statuses.put(TVARVCPreffixes.RENEGOTIATED, getFilter(TVARVCPreffixes.RENEGOTIATED));
			
			statuses.put("ORDER:"+TVARVCPreffixes.RENEGOTIATED, getFilter(TVARVCPreffixes.RENEGOTIATED,true,false,false,false));
			
			statuses.put(TVARVCPreffixes.BLOCKED, getFilter(TVARVCPreffixes.BLOCKED));
			statuses.put(TVARVCPreffixes.TO_BE_CONFIRMED, getFilter(TVARVCPreffixes.TO_BE_CONFIRMED));
			statuses.put(TVARVCPreffixes.ADVANCED_BOOKING, getFilter(TVARVCPreffixes.ADVANCED_BOOKING));
			statuses.put(TVARVCPreffixes.IN_PROCESS, getFilter(TVARVCPreffixes.IN_PROCESS));
			statuses.put(TVARVCPreffixes.ORDER_ON_HOLD, getFilter(TVARVCPreffixes.ORDER_ON_HOLD));
			statuses.put(TVARVCPreffixes.COMPLETED, getFilter(TVARVCPreffixes.COMPLETED));
			statuses.put(TVARVCPreffixes.COMPLETED_DELAYED, getFilter(TVARVCPreffixes.COMPLETED_DELAYED));
			
			statuses.put(TVARVCPreffixes.PUMP_SERVICE, getFilter(TVARVCPreffixes.PUMP_SERVICE));
			statuses.put(TVARVCPreffixes.VISIBLE_AT_PLANT, getFilter(TVARVCPreffixes.VISIBLE_AT_PLANT));
			statuses.put(TVARVCPreffixes.SELF_COLLECT, getFilter(TVARVCPreffixes.SELF_COLLECT));
			statuses.put(TVARVCPreffixes.CALLBACK, getFilter(TVARVCPreffixes.CALLBACK));
			
		}
		public static var logger:ILogger = LoggerFactory.getLogger("StatusHelper");
		public static function getFilter(status:String,order:Boolean=true,load:Boolean=true,itemCategory:Boolean=true,extras:Boolean=true):ArrayCollection{
			var filters:ArrayCollection =  new ArrayCollection();
			if (order){
				getFilterCondition("orderStatus",TVARVCPreffixes.ORDER_PREFFIX ,status,FilterCondition.EQUALS,filters);
			}
			if (load){
				getFilterCondition("loadStatus",TVARVCPreffixes.LOAD_PREFFIX ,status,FilterCondition.EQUALS,filters);
			}
			if (itemCategory){
				getFilterCondition("itemCategory",TVARVCPreffixes.ITEMCATEGORY_PREFFIX ,status,FilterCondition.EQUALS,filters);
			}
			/*
			if (extras){
			var arr :ArrayCollection = getTVARVCSelection(TVARVCPreffixes.EXTRA_PREFFIX + status);
			if (arr != null){
			for (var i:int = 0 ; i < arr.length ; i ++){
			var temp:TVARVC = arr.getItemAt(i) as TVARVC;
			if (temp != null){
			var filter:FilterCondition = FilterHelper.getFilterConditionTVARVC(temp.high,new ArrayCollection([temp]),FilterCondition.EQUALS);
			filters.addItem(filter);
			}
			}
			}
			}
			*/
			return filters;
		}
		public static function getFilterCondition(field:String,preffix:String,status:String,condition:String,filters:ArrayCollection):FilterCondition{
			var selection :ArrayCollection = getTVARVCSelection(preffix + status);
			
			var filter:FilterCondition = null;
			if (selection != null){
				filter = FilterHelper.getFilterConditionTVARVC(field,selection,condition);
			}
			if (filters != null && filter != null){
				filters.addItem(filter);
			}
			return filter;
			
		}
		
		
		
		public static function matchStatus(payload:Object,status:String):Boolean {
			
			var filter:ArrayCollection = statuses.get(status) as ArrayCollection;
			if (filter != null){
				//var info:ArrayCollection = new ArrayCollection();
				
				var result:Boolean =  FilterHelper.matchFilters(payload,filter);
				//logger.error("status:"+status+"---result:" +result+"---"+ ReflectionHelper.object2AS(info));
				return result;
			}
			//			return payload.orderStatus == "CNCO" 
			//				&& payload.loadStatus == "CNCL";
			return false;
		}
		
		
		
		public static function isCancelled(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.CANCELLED);
			return payload.orderStatus == "CNCO" && payload.loadStatus == "CNCL";
		}
		public static function isConfirmed(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.CONFIRMED);
			return payload.orderStatus == "CONF" 
				&& payload.loadStatus == "NOST"
				&& (payload.itemCategory == "ZTC1" && payload.itemCategory == "ZTX0");
		}
		public static function isDelayed(payload:OrderLoad):Boolean {
			
			return  matchStatus(payload,TVARVCPreffixes.DELAYED) && payload.isDelayed; 
			
			
			return (payload.orderStatus == "CONF" || payload.orderStatus == "TBCO") 
			&& (payload.loadStatus == "NOST" || payload.loadStatus == "TBCL")
				&& (payload.itemCategory == "ZTC1")
				&& isDelayedPayload(payload);
		}
		
		
		public static  function isDelayedPullable(payload:OrderLoad):Boolean{
			return isDelayedPayloadTime(payload) && isDelayedPayloadStatus(payload);
			
		}
		public static  function isDelayedPayloadStatus(payload:OrderLoad):Boolean{
			return FilterHelper.matchFilters(
				payload,
				new ArrayCollection([
					/*	FilterHelper.getFilterConditionTVARVC(
					"orderStatus",
					getTVARVCSelection(TVARVCConstants.DELAY_PULL_ORDER_STATUS),
					FilterCondition.EQUALS),
					*/
					FilterHelper.getFilterConditionTVARVC(
						"loadStatus",
						getTVARVCSelection(TVARVCConstants.DELAY_PULL_LOAD_STATUS),
						FilterCondition.EQUALS)
				]));
			
			
		}
		
		public static  function isDelayedPayloadTime(payload:OrderLoad):Boolean{
			
			//var now:Date =  new Date();
			var now:Date=TimeZoneHelper.getServer();
			if (payload == null || payload.startTime == null  ){
				return false;
			}
			var ldTime:Date = payload.startTime;
			var nowTime:Number = now.getTime();//now.getHours()*60*60 + now.getMinutes()*60 + now.getSeconds();
			var loadTime:Number = ldTime.getTime();//ldTime.getHours()*60*60 + ldTime.getMinutes()*60 + ldTime.getSeconds();
			//Alert.show("nowTime("+FlexDateHelper.getTimestampString(now)+"):"+nowTime+"::"+"loadTime("+FlexDateHelper.getTimestampString(ldTime)+"):"+loadTime);
			return nowTime > loadTime;
			
			
		}
		
		
		public static function isRenegotiated(payload:OrderLoad):Boolean {
			return matchStatus(payload,TVARVCPreffixes.RENEGOTIATED);
			return payload.orderStatus == "RNGT" 
				&& (payload.loadStatus == "NOST" || payload.loadStatus == "TBCL" || payload.loadStatus == "ASSG")
				&& (payload.itemCategory == "ZTC1");
			
		}
		
		public static function isOrderRenegotiated(payload:Object):Boolean {
			
			return matchStatus(payload,"ORDER:"+TVARVCPreffixes.RENEGOTIATED);
			
			return payload.orderStatus == "RNGT" ;
		}
		
		
		public static function isBlocked(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.BLOCKED);
			return payload.orderStatus == "BCLK" 
				&& (payload.loadStatus == "NOST" || payload.loadStatus == "TBCL")
				&& (payload.itemCategory == "ZTC1");
		}
		public static function isToBeConfirmed(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.TO_BE_CONFIRMED);
			return payload.orderStatus == "TBCO" 
				&& (payload.loadStatus == "TBCL")
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0");
		}
		public static function isAdvancedBooking(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.ADVANCED_BOOKING);
			return payload.orderStatus == "ADBO" 
				&& (payload.loadStatus == "NOST" || payload.loadStatus == "TBCL")
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0");
		}
		public static function isInProcess(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.IN_PROCESS);
			return payload.orderStatus == "INPC" 
				&& (payload.loadStatus == "ASSG" || payload.loadStatus == "LDNG" || payload.loadStatus == "TJST" || payload.loadStatus == "OJOB" || payload.loadStatus == "UNLD" || payload.loadStatus == "CMPL")
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0");
		}
		public static function isOrderOnHold(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.ORDER_ON_HOLD);
			return payload.orderStatus == "HOLD" 
				&& (payload.loadStatus == "HOLI")
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0");
			
		}
		public static function isCompleted(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.COMPLETED);
			return payload.orderStatus == "CMPO" 
				&& (payload.loadStatus == "CMPL")
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0");
			
		}
		public static function isCompletedDelayed(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.COMPLETED_DELAYED) && payload.completedDelayed;
			return payload.orderStatus == "CMPO" 
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0")
				&& payload.completedDelayed;
		}
		
		
		
		public static function isConstructionProduct(payload:OrderLoad):Boolean{
			
			
			return false;
		}
		public static function isPumpingService(payload:OrderLoad):Boolean{
			
			return matchStatus(payload,TVARVCPreffixes.PUMP_SERVICE);
			return payload.orderStatus == "CONF" 
				&& (payload.loadStatus == "NOST")
				&& (payload.itemCategory == "ZTX0");
		}
		
		public static function isVisibleAtPlant(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.VISIBLE_AT_PLANT)
			&& isDistributedPlant(payload)
				&& hasCrossPlantVisibilityLine(payload);
			return payload.orderStatus == "CONF" 
				&& (payload.loadStatus == "NOST")
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0")
				&& isDistributedPlant(payload)
				&& hasCrossPlantVisibilityLine(payload);
		}
		
		/**
		 * If operative mode = “Distributed” then:
		 * If loading time  >= current time (See point 19) and <= Plant Visibility Limit (see point 20)
		 * then Select TVARVC Where name = with “RMS_DFE_color _load_distributed” color is field low 
		 */
		public static function isDistributedPlant(payload:OrderLoad):Boolean {
			return services.getFlashIslandService().getPlantType(payload.plant) == getTVARVCParam(TVARVCConstants.PLOPM_D).low;
		}
		
		
		public static function hasCrossPlantVisibilityLine(payload:OrderLoad):Boolean {
			if(payload.startTime==null){
				return false;
			}
			var plantVisibilityLineMilliSeconds:Number = GanttServiceReference.getParamNumber(ParamtersConstants.PLANT_VIS_AREA_L)* 60 * 1000;
			//var now:Date =  new Date();
			var now:Date=TimeZoneHelper.getServer();
			var plantVisibilityTime:Number = now.getTime() + plantVisibilityLineMilliSeconds;
			var taskTime:Number = payload.startTime.time;
			return taskTime < plantVisibilityTime;
		}
		
		public static function isSelfCollect(payload:OrderLoad):Boolean {
			
			return matchStatus(payload,TVARVCPreffixes.SELF_COLLECT) 
			&& FilterHelper.matchFilters(
				payload,
				new ArrayCollection([FilterHelper.getFilterConditionTVARVC(
					"shipConditions",
					getTVARVCSelection(TVARVCConstants.EXTRA_SELF_COLLECT),
					FilterCondition.EQUALS
				)])
			)
				;
			return (payload.orderStatus == "CONF" || payload.orderStatus == "TBCO")
			&& payload.shipConditions == "02"
				&& (payload.loadStatus == "NOST" || payload.loadStatus == "TBCL")
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0");
			
		}
		public static function isCallback(payload:OrderLoad):Boolean {
			return matchStatus(payload,TVARVCPreffixes.CALLBACK) && payload.renegFlag;
			
			return (payload.orderStatus == "CONF" || payload.orderStatus == "TBCO" || payload.orderStatus == "INPC")
			&& payload.renegFlag
				&&  payload.loadStatus == "TBCL"
				&& (payload.itemCategory == "ZTC1" || payload.itemCategory == "ZTX0");
		}
	}
}