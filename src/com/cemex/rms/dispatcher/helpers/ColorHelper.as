package com.cemex.rms.dispatcher.helpers
{
	import com.cemex.rms.common.filters.FilterHelper;
	import com.cemex.rms.common.flashislands.vo.TVARVC;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ColorSetting;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;

	//import mx.controls.Alert;
	
	
	
	
	public class ColorHelper extends GanttServiceReference
	{
		
		public static var shadowColors:Array=[0xFFFFFF,0x999999];
		public static var shadowAlphas:Array=[.5,.2];
		
		private static var colorMap:DictionaryMap;
		public function ColorHelper()
		{
		}
		
		private static var _pallete:ArrayCollection = null;
		public static function getPallete():ArrayCollection{
			
			if (_pallete == null) {
				
				var temp:ArrayCollection = ReflectionHelper.getConstNames(TVARVCConstants,"COLOR_");
				_pallete = new ArrayCollection();
				
				for (var i:int = 0 ;  i < temp.length ; i ++ ) {
					_pallete.addItem(TVARVCConstants[temp.getItemAt(i)]);
				}
			}
			return _pallete;
		}
		
		//public static var service:IGanttServiceFactory;
		
		
		public static function init():void{
		
			
			colorMap =  GanttServiceReference.getColorMap();
			
			
			var colors:ArrayCollection = getPallete();
			_colormap = new DictionaryMap;
			for (var i:int = 0 ; i < colors.length ; i ++){
				
				var name:String = ""+colors.getItemAt(i); 
				
				var value:TVARVC = getTVARVCParam(name);
				if (value != null){
					
					var valuex:String = value.low;
					var scopes:Array = valuex.split("|");
					
					var  color:ColorSetting;
					if (scopes.length == 1) {
						
						color = getColorSettings(valuex);
						color.status = name;
						_colormap.put(TVARVCPreffixes.LOAD_SCOPE + ":" + name,color);				
					}
					else if (scopes.length == 2){
						
						color = getColorSettings(scopes[0]);
						color.status = name;
						_colormap.put(TVARVCPreffixes.LOAD_SCOPE + ":" + name,color);
						
						color = getColorSettings(scopes[1]);
						color.status = name;
						_colormap.put(TVARVCPreffixes.ORDER_SCOPE + ":" + name,color);
					}
				}
			}
			
		} 
		
		
		private static var _colormap:DictionaryMap ;
		public static function getColorMap():DictionaryMap{
			
			
				return _colormap;
			
		}
		
		public static function getColorSettings(color:String):ColorSetting{
			var result:ColorSetting = new ColorSetting();
			
			var split:Array = color.split("/");
			
			
			var value1:String = colorMap.get(""+split[0]);
			var value2:String = colorMap.get(""+split[1]);
			
			
			result.fillColor =  Number(value1);
			result.borderColor = Number(value2);
			result.borderThickness = 2;
			
			
			return result;
		}
		
		public static  function getColorFrom(status:String,scope:String):ColorSetting{
			
			var colorSetting:ColorSetting = null;
			if (getColorMap() != null){
				colorSetting =  getColorMap().get(scope+":"+status) as ColorSetting;
			}
			if (colorSetting == null){
				//Alert.show("Color not Found:"+scope+":"+status);
				colorSetting = getDefaultColor();
			}
			return colorSetting;
		}
		
		public static function getDefaultColor():ColorSetting{
			var colorSetting:ColorSetting = new ColorSetting();
			colorSetting.borderColor = 0xF0C772;
			colorSetting.fillColor =   0xF0C772;
			colorSetting.borderThickness = 2;
			return colorSetting;
		}
		
		private static  var logger:ILogger = LoggerFactory.getLogger("DispatcherColorHelper");
		
		
		
		public static  function getColor(payload:OrderLoad, scope:String):ColorSetting{
			
			payload.isDelayed =  StatusHelper.isDelayedPullable(payload);
			
			/*if (StatusHelper.isCancelled(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_CANCELLED,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if ( StatusHelper.isPumpingService(payload) ){
				return getColorFrom(TVARVCConstants.COLOR_PUMP_SERVICE,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if ( StatusHelper.isVisibleAtPlant(payload) ){
				return getColorFrom(TVARVCConstants.COLOR_VISIBLE_AT_PLANT,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if (StatusHelper.isSelfCollect(payload)){
				return getColorFrom(TVARVCConstants.COLOR_SELF_COLLECT,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if ( StatusHelper.isCallback(payload) ){
				return getColorFrom(TVARVCConstants.COLOR_CALLBACK,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if (StatusHelper.isDelayed(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_DELAYED,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if (StatusHelper.isRenegotiated(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_RENEGOTIATED,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if (StatusHelper.isBlocked(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_BLOCKED,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if (StatusHelper.isInProcess(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_IN_PROCESS,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if ( StatusHelper.isOrderOnHold(payload) ){
				return getColorFrom(TVARVCConstants.COLOR_ORDER_ON_HOLD,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if (StatusHelper.isAdvancedBooking(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_ADVANCED_BOOKING,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if (StatusHelper.isConfirmed(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_CONFIRMED,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if (StatusHelper.isToBeConfirmed(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_TO_BE_CONFIRMED,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if ( StatusHelper.isCompleted(payload) ){
				return getColorFrom(TVARVCConstants.COLOR_COMPLETED,TVARVCPreffixes.LOAD_SCOPE);
			}
			else if ( StatusHelper.isCompletedDelayed(payload) ){
				return getColorFrom(TVARVCConstants.COLOR_COMPLETED_DELAYED,TVARVCPreffixes.LOAD_SCOPE);
			}
			else {
				//Alert.show(ReflectionHelper.object2XML(payload,"PayloadColor"));
			}*/
			if (StatusHelper.isDelayed(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_DELAYED,TVARVCPreffixes.LOAD_SCOPE);
			} else {
				return getDefaultColor();
				
			}
		}
		/*
		private static  function getOrderColor(payload:OrderLoad, scope:String):ColorSetting{
			
			if (StatusHelper.isCancelled(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_CANCELLED,scope);
			}
			else if (StatusHelper.isDelayed(payload)) {
				return getColorFrom(TVARVCConstants.COLOR_DELAYED,scope);
			}
			else
			
		
			}
		}
			*/
		/*
		public static  function getLoadColor(payload:OrderLoad):ColorSetting{
			
			
			else {
				return null;//getColorFrom(TVARVCConstants.COLOR_CONFIRMED,TVARVCPreffixes.LOAD_SCOPE);
			}
		}
		*/
	
		
		
		
	}
}