package com.cemex.rms.dispatcher.constants
{
	public class TVARVCPreffixes
	{
		public function TVARVCPreffixes()
		{
		}
		
		public static const LOAD_SUFFIX:String = "_L";
		public static const ORDER_SUFFIX:String = "_O";
		public static const ITEMCATEGORY_SUFFIX:String = "_I";
		public static const EXTRA_SUFFIX:String = "_X";
		
		
		public static const COLOR_PREFFIX:String = "C_";
		public static const LOAD_PREFFIX:String = "L_";
		public static const ORDER_PREFFIX:String = "O_";
		public static const ITEMCATEGORY_PREFFIX:String = "I_";
		public static const EXTRA_PREFFIX:String = "X_";
		
		
		public static const ASSIGNED_LOADS_VIEW_ID:String = "FXD_ASL"; 
		public static const LOADS_PER_ORDER_VIEW_ID:String = "FXD_LPO"; 
		public static const LOADS_PER_VEHICLE_VIEW_ID:String = "FXD_LPV";
		public static const PLANNING_SCREEN_VIEW_ID:String="FXD_PS";
		public static const SERVICE_AGENT_SCREEN_VIEW_ID:String="FXD_SAS";
		
		
		
		public static const LOAD_SCOPE:String = "LOAD";
		public static const ORDER_SCOPE:String = "ORDER";
		
		
		public static const CANCELLED:String 			= 'CANCELL';
		public static const CONFIRMED:String 			= 'CONFIRM';
		public static const DELAYED:String 				= 'DELAYED';
		public static const RENEGOTIATED:String 		= 'RENEGOT';
		public static const BLOCKED:String 				= 'BLOCKED';
		public static const TO_BE_CONFIRMED:String 		= 'TO_BE_C';
		public static const ADVANCED_BOOKING:String 	= 'ADVANCE';
		public static const IN_PROCESS:String 			= 'IN_PROC';
		public static const ORDER_ON_HOLD:String 		= 'ORDER_O';
		public static const COMPLETED:String 			= 'COMPLET';
		public static const COMPLETED_DELAYED:String 	= 'COMPL_D';
		
		public static const PUMP_SERVICE:String 		= 'PUMP_SE';
		public static const VISIBLE_AT_PLANT:String 	= 'VISI_PL';
		public static const SELF_COLLECT:String 		= 'SELF_CO';
		public static const CALLBACK:String 			= 'CALL_BK';

	}
}