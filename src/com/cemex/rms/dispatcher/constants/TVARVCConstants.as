package com.cemex.rms.dispatcher.constants
{
	public class TVARVCConstants
	{
		public function TVARVCConstants()
		{
		}
		
		
		
		
		public static const COLLECT_ORDER:String = "COLL_ORDE";
		public static const PUMP_SERVICE:String = "PUMP";
		public static const CONSTRUCTION_PRODUCT:String = "CPTYPE";
		public static const VALUE_ADDED:String = "ADDITION";
		public static const SHIPCOND:String = "SHIPCOND";
		
		public static const PLANT_TOOLTIP_LOAD_FILTER_DELIVERIED:String = "PTT_L_DEL";
		public static const PLANT_TOOLTIP_LOAD_FILTER_CONFIRMED:String = "PTT_L_CON";
		public static const PLANT_TOOLTIP_LOAD_FILTER_TO_BE_CONFIRMED:String = "PTT_L_TBC";
		public static const PLANT_TOOLTIP_LOAD_FILTER_CANCELED:String = "PTT_L_CNC";
		public static const PLANT_TOOLTIP_LOAD_FILTER_ASSIGNED:String = "PTT_L_ASS";
		
		
		public static const VEHICLE_TOOLTIP_LOAD_FILTER_DELIVERIED:String = "VTT_L_DEL";
		public static const VEHICLE_TOOLTIP_LOAD_FILTER_CONFIRMED:String = "VTT_L_CON";
		public static const VEHICLE_TOOLTIP_LOAD_FILTER_TO_BE_CONFIRMED:String = 	"VTT_L_TBC";
		
		
		
		public static const DELAY_PULL_ORDER_STATUS:String = 	"DLY_PLL_O";
		public static const DELAY_PULL_LOAD_STATUS:String = 	"DLY_PLL_L";

		
		public static const MAP_COLOR_CATALOG:String = "COLOR_CAT";
		
		/*
		public static const COLOR_CANCELLED:String 			= "C_CANCELL"; // USADO
		public static const COLOR_CONFIRMED:String 			= "C_CONFIRM"; // USADO
		public static const COLOR_DELAYED:String 			= "C_DELAYED"; // USADO
		public static const COLOR_RENEGOTIATED:String 		= "C_RENEGOT"; // USADO
		public static const COLOR_BLOCKED:String 			= "C_BLOCKED"; // USADO
		public static const COLOR_TO_BE_CONFIRMED:String 	= "C_TO_BE_C"; // USADO
		public static const COLOR_ADVANCED_BOOKING:String 	= "C_ADVANCE"; // USADO
		public static const COLOR_IN_PROCESS:String 		= "C_IN_PROC"; // USADO
		public static const COLOR_ORDER_ON_HOLD:String 		= "C_ORDER_O"; // USADO
		public static const COLOR_COMPLETED:String 			= "C_COMPLET"; // USADO
		public static const COLOR_COMPLETED_DELAYED:String 	= "C_COMPL_D"; // USADO
		
		public static const COLOR_PUMP_SERVICE:String 		= "C_PUMP_SE"; // USADO
		public static const COLOR_VISIBLE_AT_PLANT:String 	= "C_VISI_PL"; // USADO
		public static const COLOR_SELF_COLLECT:String 		= "C_SELF_CO"; // USADO
		public static const COLOR_CALLBACK:String 			= "C_CALL_BK"; // USADO
		
		*/
		public static const COLOR_CANCELLED:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.CANCELLED;
		public static const COLOR_CONFIRMED:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.CONFIRMED;
		public static const COLOR_DELAYED:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.DELAYED;
		public static const COLOR_RENEGOTIATED:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.RENEGOTIATED;
		public static const COLOR_BLOCKED:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.BLOCKED;
		public static const COLOR_TO_BE_CONFIRMED:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.TO_BE_CONFIRMED;
		public static const COLOR_ADVANCED_BOOKING:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.ADVANCED_BOOKING;
		public static const COLOR_IN_PROCESS:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.IN_PROCESS;
		public static const COLOR_ORDER_ON_HOLD:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.ORDER_ON_HOLD;
		public static const COLOR_COMPLETED:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.COMPLETED;
		public static const COLOR_COMPLETED_DELAYED:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.COMPLETED_DELAYED;
		public static const COLOR_PUMP_SERVICE:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.PUMP_SERVICE;
		public static const COLOR_VISIBLE_AT_PLANT:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.VISIBLE_AT_PLANT;
		public static const COLOR_SELF_COLLECT:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.SELF_COLLECT;
		public static const COLOR_CALLBACK:String = TVARVCPreffixes.COLOR_PREFFIX+ TVARVCPreffixes.CALLBACK;

		
		public static const PLOPM_A:String = "PLOPM_A"; // USADO Automatizado
		public static const PLOPM_C:String = "PLOPM_C"; // USADO Centralizado
		public static const PLOPM_D:String = "PLOPM_D"; // USADO Distribuido
		
		
		public static const VEHICLES_STATUS_AVAILABLE:String = "AVLB"; // USADO Distribuido
		
		
		
		
		
		// LAs referencias de Abajo son simplemente para validar que las variables existan y esten llegando a Flex
		// variables de la tvarv
		
		
		
		
		public static const ASSIGNED_LOADS_VIEW_ID_O:String = TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID + TVARVCPreffixes.ORDER_SUFFIX; 
		public static const LOADS_PER_ORDER_VIEW_ID_O:String =  TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID + TVARVCPreffixes.ORDER_SUFFIX
		public static const LOADS_PER_VEHICLE_VIEW_ID_O:String = TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID + TVARVCPreffixes.ORDER_SUFFIX
		
		
		public static const ASSIGNED_LOADS_VIEW_ID_L:String = TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID + TVARVCPreffixes.LOAD_SUFFIX;  
		public static const LOADS_PER_ORDER_VIEW_ID_L:String = TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID + TVARVCPreffixes.LOAD_SUFFIX;
		public static const LOADS_PER_VEHICLE_VIEW_ID_L:String = TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID + TVARVCPreffixes.LOAD_SUFFIX;
		

		public static const ORDER_CANCELLED:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.CANCELLED;
		public static const ORDER_CONFIRMED:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.CONFIRMED;
		public static const ORDER_DELAYED:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.DELAYED;
		public static const ORDER_RENEGOTIATED:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.RENEGOTIATED;
		public static const ORDER_BLOCKED:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.BLOCKED;
		public static const ORDER_TO_BE_CONFIRMED:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.TO_BE_CONFIRMED;
		public static const ORDER_ADVANCED_BOOKING:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.ADVANCED_BOOKING;
		public static const ORDER_IN_PROCESS:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.IN_PROCESS;
		public static const ORDER_ORDER_ON_HOLD:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.ORDER_ON_HOLD;
		public static const ORDER_COMPLETED:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.COMPLETED;
		public static const ORDER_COMPLETED_DELAYED:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.COMPLETED_DELAYED;
		public static const ORDER_PUMP_SERVICE:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.PUMP_SERVICE;
		public static const ORDER_VISIBLE_AT_PLANT:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.VISIBLE_AT_PLANT;
		public static const ORDER_SELF_COLLECT:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.SELF_COLLECT;
		public static const ORDER_CALLBACK:String = TVARVCPreffixes.ORDER_PREFFIX+ TVARVCPreffixes.CALLBACK;

		public static const LOAD_CANCELLED:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.CANCELLED;
		public static const LOAD_CONFIRMED:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.CONFIRMED;
		public static const LOAD_DELAYED:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.DELAYED;
		public static const LOAD_RENEGOTIATED:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.RENEGOTIATED;
		public static const LOAD_BLOCKED:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.BLOCKED;
		public static const LOAD_TO_BE_CONFIRMED:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.TO_BE_CONFIRMED;
		public static const LOAD_ADVANCED_BOOKING:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.ADVANCED_BOOKING;
		public static const LOAD_IN_PROCESS:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.IN_PROCESS;
		public static const LOAD_ORDER_ON_HOLD:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.ORDER_ON_HOLD;
		public static const LOAD_COMPLETED:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.COMPLETED;

		public static const LOAD_PUMP_SERVICE:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.PUMP_SERVICE;
		public static const LOAD_VISIBLE_AT_PLANT:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.VISIBLE_AT_PLANT;
		public static const LOAD_SELF_COLLECT:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.SELF_COLLECT;
		public static const LOAD_CALLBACK:String = TVARVCPreffixes.LOAD_PREFFIX+ TVARVCPreffixes.CALLBACK;

		
		public static const ITEMCATEGORY_CONFIRMED:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.CONFIRMED;
		public static const ITEMCATEGORY_DELAYED:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.DELAYED;
		public static const ITEMCATEGORY_RENEGOTIATED:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.RENEGOTIATED;
		public static const ITEMCATEGORY_BLOCKED:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.BLOCKED;
		public static const ITEMCATEGORY_TO_BE_CONFIRMED:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.TO_BE_CONFIRMED;
		public static const ITEMCATEGORY_ADVANCED_BOOKING:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.ADVANCED_BOOKING;
		public static const ITEMCATEGORY_IN_PROCESS:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.IN_PROCESS;
		public static const ITEMCATEGORY_ORDER_ON_HOLD:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.ORDER_ON_HOLD;
		public static const ITEMCATEGORY_COMPLETED:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.COMPLETED;
		public static const ITEMCATEGORY_COMPLETED_DELAYED:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.COMPLETED_DELAYED;
		public static const ITEMCATEGORY_PUMP_SERVICE:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.PUMP_SERVICE;
		public static const ITEMCATEGORY_VISIBLE_AT_PLANT:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.VISIBLE_AT_PLANT;
		public static const ITEMCATEGORY_SELF_COLLECT:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.SELF_COLLECT;
		public static const ITEMCATEGORY_CALLBACK:String = TVARVCPreffixes.ITEMCATEGORY_PREFFIX+ TVARVCPreffixes.CALLBACK;

		
		public static const EXTRA_SELF_COLLECT:String = TVARVCPreffixes.EXTRA_PREFFIX+ TVARVCPreffixes.SELF_COLLECT;

	}
}