package com.cemex.rms.dispatcher.services
{
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.dispatcher.services.flashislands.IDispatcherIsland;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherProducer;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherReceiver;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	public class GanttServiceFactory implements IGanttServiceFactory
	{
		public function GanttServiceFactory()
		{
		}
		
		
		
		//[Inject]
		public var serviceFactory:IServiceFactory;
		
		
		public  function getModel():IServiceModel{
			return serviceFactory.model;
		}
		
		public function getFlashIslandService():IDispatcherIsland {
			var result:IDispatcherIsland = serviceFactory.getService("DispatchIsland") as IDispatcherIsland;
			if (result == null || !result.isReady()){
				result = serviceFactory.getService("DispatchIslandMock") as IDispatcherIsland;
			}
			
			return result;
		}
		
		
		
		public function getVehiclePIReceiver():IDispatcherReceiver {
			var receiver:IDispatcherReceiver = serviceFactory.getService("VehiclePI_CR") as IDispatcherReceiver;
			return receiver;
		}
		public function getSalesOrderPIReceiver():IDispatcherReceiver {
			var receiver:IDispatcherReceiver = serviceFactory.getService("SalesOrderPI_CR") as IDispatcherReceiver;
			return receiver;
		}
		public function getProductionStatusPIReceiver():IDispatcherReceiver {
			var receiver:IDispatcherReceiver = serviceFactory.getService("ProductionStatusPI_CR") as IDispatcherReceiver;
			return receiver;
		}
		public function getPlantPIReceiver():IDispatcherReceiver {
			var receiver:IDispatcherReceiver = serviceFactory.getService("PlantPI_CR") as IDispatcherReceiver;
			return receiver;
		}
		/*
		public function getDispatchPIReceiver():IDispatcherReceiver {
		var receiver:IDispatcherReceiver = serviceFactory.getService("DispatchPIReceiver") as IDispatcherReceiver;
		return receiver;
		}
		public function getDispatchInternalReceiver():IDispatcherReceiver {
			
			
			var receiver:IDispatcherReceiver = 
				serviceFactory.getService("DispatchInternalReceiver") as IDispatcherReceiver;
			return receiver;
		}
		public function getDispatchInternalProducer():IDispatcherProducer {
			
			var producer:IDispatcherProducer = serviceFactory.getService("DispatchInternalProducer") as IDispatcherProducer;
			return producer;
		}
		*/
	}
}