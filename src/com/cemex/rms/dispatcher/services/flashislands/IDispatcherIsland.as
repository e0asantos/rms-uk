package com.cemex.rms.dispatcher.services.flashislands
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.common.flashislands.vo.TVARVC;
	import com.cemex.rms.common.services.IService;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.services.flashislands.vo.DateText;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Plant;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;

	public interface IDispatcherIsland extends IService
	{
		function getColorPalette():Object;
		function isVisibleField(view:String,event:String,operativeMode:String):Boolean;
		function isEnabledField(view:String,event:String,operativeMode:String):Boolean;
		
		function getProductionData(payload:*):ProductionData; 
		function setProductionData(payload:ProductionData):void;
		function getUserRole():String;
		function setUserRole(role:String):void;
		function getUser():String;
		function getPlants():ArrayCollection;
		function getPlantNames():ArrayCollection;
		function getPlantsPlain():ArrayCollection;
		function getEquipmentsPlain():ArrayCollection;
		function getInitialConfig():Object;
		function getPlanningLoads():ArrayCollection;
		
		function updateEquipment(vehicle:Equipment):Object;
			
		function getPlantType(plantId:String):String;
		function setPlantType(plantId:String,plantType:String,statusCode:String):void;
		function getPlantInfo(plantId:String):Plant;
		
		function isEnabledParameter():Boolean;
		//function setPlantType(type:String):void;
		
		
		function getTVARVCParam(name:String):TVARVC ;
		function getTVARVCSelection(name:String):ArrayCollection;
		
		function getOTRValue(name:String):String;
		function getParamString(param:String,plant:String=null):String;
		function getParamNumber(param:String,plant:String=null):Number;
		
		
		
		function refreshData():void;
		
		
		function dispatchWDEvent(fio:IFlashIslandEventObject):void;
		
		function getColorMap():DictionaryMap;
		
		function getDateText(date:String):DateText;
	}
}