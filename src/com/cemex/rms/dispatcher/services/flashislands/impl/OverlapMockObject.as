package com.cemex.rms.dispatcher.services.flashislands.impl
{
	import mx.collections.ArrayCollection;

	public class MockObject
	{
		public function MockObject()
		{
		}
		
		public var data: Object =
			
			/*Object*/{
				"DATA":/*sap.core.wd.context::WDContextNode*/{
					"PRODUCTION_DATA":new ArrayCollection([])
					,"RETURN_LOG":new ArrayCollection([])
					,"leadselection":/*String*/ "0"
					,"name":/*String*/ "DATA"
				}
				,"DS_SORDER":new ArrayCollection([
					/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "14:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "00:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000209
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ "X"
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000209
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "14:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "00:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000209
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ "X"
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000209
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "14:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "3"
								,"DELIVERY_TIME":/*String*/ "00:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000209
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ "X"
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000209
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000035974"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 21
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "13:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "10:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000069
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000069
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036152"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 7
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "ALMOLOYA DE JUAREZ"
							,"HOUSE_NO_STREET":/*String*/ "CARRETERA TOLUCA ATLACOMULCO KM20"
							,"JOB_SITE_ID":/*String*/ "0065010054"
							,"JOB_SITE_NAME":/*String*/ "AMPLIACION PENAL ALMOLOYA DE JUAREZ"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "12:05:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "11:45:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "12:10:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "11:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L2"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "3"
								,"DELIVERY_TIME":/*String*/ "13:35:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "13:15:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L3"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "4"
								,"DELIVERY_TIME":/*String*/ "13:45:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1004"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "13:25:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L4"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "5"
								,"DELIVERY_TIME":/*String*/ "13:55:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1005"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "13:35:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L5"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036155"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "RNGT"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 35
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "TRADECO INFRAESTRUCTURA, S.A. DE"
						,"SOLD_TO_NUMBER":/*String*/ "0050160367"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "ALMOLOYA DE JUAREZ"
							,"HOUSE_NO_STREET":/*String*/ "CARRETERA TOLUCA ATLACOMULCO KM20"
							,"JOB_SITE_ID":/*String*/ "0065010054"
							,"JOB_SITE_NAME":/*String*/ "AMPLIACION PENAL ALMOLOYA DE JUAREZ"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "13:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "12:40:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "13:10:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "12:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L2"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "3"
								,"DELIVERY_TIME":/*String*/ "13:20:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "13:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L3"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "4"
								,"DELIVERY_TIME":/*String*/ "13:30:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1004"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "13:10:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L4"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 0
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "01:10:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "5"
								,"DELIVERY_TIME":/*String*/ "13:40:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ "MTT-234"
								,"EQUIP_NUMBER":/*String*/ "000000000011004222"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1005"
								,"LICENSE":/*String*/ "MTT-234"
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "13:20:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L5"
								,"LOAD_STATUS":/*String*/ "ASSG"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000349
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "ASSN"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000349
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ "ID 2218"
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036183"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 35
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "TRADECO INFRAESTRUCTURA, S.A. DE"
						,"SOLD_TO_NUMBER":/*String*/ "0050160367"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ "X"
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "03:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "14:25:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "12:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000139
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000139
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:05:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "03:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "14:30:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "12:55:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L2"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000139
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000139
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:05:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036190"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "12:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "10:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000069
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000069
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036195"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 7
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "12:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "09:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000069
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000069
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036199"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 7
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "02"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "03:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "16:25:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ "MVN-982"
								,"EQUIP_NUMBER":/*String*/ "000000000011004223"
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ "MVN-982"
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "14:50:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "ASSG"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000139
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ "ASSN"
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000139
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:05:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ "ID 2672"
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "03:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "16:30:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "14:55:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L2"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000139
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000139
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:05:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036200"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "INPC"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 14
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ "X"
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "13:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "09:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000069
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000069
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036203"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 7
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "12:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "09:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000209
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000209
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "12:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "09:15:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:15:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L2"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000209
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000209
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "12:11:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "3"
								,"DELIVERY_TIME":/*String*/ "09:30:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:30:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L3"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000209
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000209
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036204"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 21
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "03:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "11:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000199
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ "X"
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000199
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "03:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "2"
								,"DELIVERY_TIME":/*String*/ "11:10:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1002"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000199
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ "X"
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000199
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 6
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "03:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "3"
								,"DELIVERY_TIME":/*String*/ "11:20:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1003"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "00:00:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ ""
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 6
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000199
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ "X"
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000199
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036205"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 20
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
					,/*sap.core.wd.context::WDContextNode*/{
						"ADDPROD":new ArrayCollection([])
						,"CONTACT_NAME1":/*String*/ ""
						,"CONTACT_NAME2":/*String*/ ""
						,"CONTACT_TEL":/*String*/ ""
						,"C_PRODUCT":/*sap.core.wd.context::WDContextNode*/{
							"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"QUANTITY":/*int*/ 0
							,"UOM":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "C_PRODUCT"
						}
						,"DELIVERY_BLOCK":/*String*/ ""
						,"FIRST_CONCRETE":/*sap.core.wd.context::WDContextNode*/{
							"DELIVERY_TIME":/*String*/ "00:00:00"
							,"DESCRIPTION":/*String*/ ""
							,"ORDER_NUMBER":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "FIRST_CONCRETE"
						}
						,"JOB_SITE":/*sap.core.wd.context::WDContextNode*/{
							"CITY":/*String*/ "TLALPAN"
							,"HOUSE_NO_STREET":/*String*/ "CUITLAHUAC 134"
							,"JOB_SITE_ID":/*String*/ "0065010066"
							,"JOB_SITE_NAME":/*String*/ "RESIDENCIAL CUITLAHUAC (SQC)"
							,"POSTAL_CODE":/*String*/ ""
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "JOB_SITE"
						}
						,"LOADS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"ADDITIONAL_FLAG":/*String*/ ""
								,"CONFIRM_QTY":/*int*/ 7
								,"CONSTRUCTION_PRODUCT":/*String*/ ""
								,"CYCLE_TIME":/*String*/ "03:40:00"
								,"DELIVERED_AMOUNT":/*int*/ 0
								,"DELIVERY_GROUP":/*String*/ "1"
								,"DELIVERY_TIME":/*String*/ "05:00:00"
								,"DRIVER":/*String*/ ""
								,"EQUIP_LABEL":/*String*/ ""
								,"EQUIP_NUMBER":/*String*/ ""
								,"HLEVEL_ITEM":/*String*/ "1000"
								,"ID_HLEVEL_ITEM":/*String*/ ""
								,"ITEM_CATEGORY":/*String*/ "ZTC1"
								,"ITEM_CURRENCY":/*String*/ "MXN"
								,"ITEM_NUMBER":/*String*/ "1001"
								,"LICENSE":/*String*/ ""
								,"LOADING_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
								,"LOADING_TIME":/*String*/ "03:25:00"
								,"LOAD_FREQUENCY":/*int*/ 0
								,"LOAD_NUMBER":/*String*/ "L1"
								,"LOAD_STATUS":/*String*/ "NOST"
								,"LOAD_UOM":/*String*/ "M3"
								,"LOAD_VOLUME":/*int*/ 7
								,"MATERIAL_DES":/*String*/ "CCO-1-100-28-1"
								,"MATERIAL_ID":/*String*/ "000000000010000792"
								,"MEP_TOTCOST":/*int*/ 10000069
								,"PLANT":/*String*/ "D074"
								,"REMAIN_FLAG":/*String*/ ""
								,"SHIPMENT_STATUS":/*String*/ ""
								,"TIME_FIX_INDICATOR":/*String*/ ""
								,"TIME_PUMP_JOBS":/*String*/ "00:00:00"
								,"TOT_COST":/*int*/ 10000069
								,"TRUCK_VOLUME":/*int*/ 7
								,"UNLOADING_TIME":/*String*/ "00:06:00"
								,"VALUE_ADDED":/*String*/ ""
								,"VEHICLE_ID":/*String*/ ""
								,"VEHICLE_TYPE":/*String*/ ""
							}
						])
						,"ORDER_NUMBER":/*String*/ "0000036207"
						,"ORDER_REASON":/*String*/ ""
						,"ORDER_STATUS":/*String*/ "CONF"
						,"ORDER_UOM":/*String*/ "M3"
						,"ORDER_VOLUME":/*int*/ 7
						,"PAYTERM_LABEL":/*String*/ ""
						,"RENEG_FLAG":/*Boolean*/ false
						,"SHIP_CONDITIONS":/*String*/ "01"
						,"SOLD_TO_NAME":/*String*/ "INMOBILIARIA BRUGEN, S.A. DE C.V."
						,"SOLD_TO_NUMBER":/*String*/ "0050160376"
						,"TEXT":new ArrayCollection([])
						,"TICKETS":new ArrayCollection([])
						,"VEHICLE_MAX_VOL":/*int*/ 7
						,"WARNING":/*String*/ ""
					}
				])
				,"IMPORT_DISSO":/*sap.core.wd.context::WDContextNode*/{
					"DATE":/*Date*/ new Date(2011,7,05,0,0,0)
					,"DATES":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"LARGE_DATE":/*String*/ "Viernes Agosto 05 2011"
							,"REF_DATE":/*String*/ "20110805"
							,"SHORT_DATE":/*String*/ "AGO 05 2011"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"LARGE_DATE":/*String*/ "Jueves Agosto 04 2011"
							,"REF_DATE":/*String*/ "20110804"
							,"SHORT_DATE":/*String*/ "AGO 04 2011"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"LARGE_DATE":/*String*/ "Sábado Agosto 06 2011"
							,"REF_DATE":/*String*/ "20110806"
							,"SHORT_DATE":/*String*/ "AGO 06 2011"
						}
					])
					,"FLASH_ISLAND_STATUS":/*String*/ "INITIAL_DATA"
					,"PLANT":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"EQUIPMENTS":new ArrayCollection([
								/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011004222"
									,"EQUIP_LABEL":/*String*/ "MTT-234"
									,"FLEET_NUM":/*String*/ "ID 2218"
									,"LICENSE_NUM":/*String*/ "MTT-234"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000011004222"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "995"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011004223"
									,"EQUIP_LABEL":/*String*/ "MVN-982"
									,"FLEET_NUM":/*String*/ "ID 2672"
									,"LICENSE_NUM":/*String*/ "MVN-982"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000011004223"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
									,"TURN_TIME":/*String*/ "15:23:27"
									,"VEHICLE_TYPE":/*String*/ "995"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011004237"
									,"EQUIP_LABEL":/*String*/ "TKE-999"
									,"FLEET_NUM":/*String*/ "ID 9989"
									,"LICENSE_NUM":/*String*/ "TKE-999"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000011004237"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "Y"
									,"TURN_DATE":/*Date*/ new Date(2011,7,05,0,0,0)
									,"TURN_TIME":/*String*/ "15:14:02"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011004238"
									,"EQUIP_LABEL":/*String*/ "TKE-777"
									,"FLEET_NUM":/*String*/ "ID 7772"
									,"LICENSE_NUM":/*String*/ "TKE-777"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000011004238"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "994"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011004244"
									,"EQUIP_LABEL":/*String*/ "RVS-123"
									,"FLEET_NUM":/*String*/ "ID 68922"
									,"LICENSE_NUM":/*String*/ "RVS-123"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000011004244"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "995"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011004245"
									,"EQUIP_LABEL":/*String*/ "RVS-993"
									,"FLEET_NUM":/*String*/ "ID 9201"
									,"LICENSE_NUM":/*String*/ "RVS-993"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000011004245"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "Y"
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "995"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "V"
									,"EQUIPMENT":/*String*/ "000000000011004249"
									,"EQUIP_LABEL":/*String*/ "RVS-533"
									,"FLEET_NUM":/*String*/ "ID 5401"
									,"LICENSE_NUM":/*String*/ "RVS-533"
									,"LOAD_VOL":/*int*/ 7
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000011004249"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "995"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000138"
									,"EQUIP_LABEL":/*String*/ "MAD 5910"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5910"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000138"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ ""
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000141"
									,"EQUIP_LABEL":/*String*/ "MAD 5913"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5913"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000141"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000143"
									,"EQUIP_LABEL":/*String*/ "MAD 5915"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5915"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000143"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ "M3"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000144"
									,"EQUIP_LABEL":/*String*/ "MAD 5920"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5920"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000144"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ ""
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000145"
									,"EQUIP_LABEL":/*String*/ "MAD 5921"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5921"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000145"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ ""
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000146"
									,"EQUIP_LABEL":/*String*/ "MAD 5922"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5922"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000146"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ ""
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000147"
									,"EQUIP_LABEL":/*String*/ "MAD 5923"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5923"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000147"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ ""
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000148"
									,"EQUIP_LABEL":/*String*/ "MAD 5924"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5924"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000148"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ ""
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ ""
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000149"
									,"EQUIP_LABEL":/*String*/ "MAD 5925"
									,"FLEET_NUM":/*String*/ ""
									,"LICENSE_NUM":/*String*/ "MAD 5925"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000149"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ ""
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "993"
									,"VOL_UNIT":/*String*/ ""
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "DUMMY"
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000151"
									,"EQUIP_LABEL":/*String*/ "DUMMIE"
									,"FLEET_NUM":/*String*/ "ID DUMMY"
									,"LICENSE_NUM":/*String*/ "DUMMIE"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000151"
									,"STATUS":/*String*/ "ASSN"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "930"
									,"VOL_UNIT":/*String*/ ""
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARRIVAL_DATE":/*null*/{}
									,"ARRIVAL_TIME":/*String*/ "00:00:00"
									,"DRIVER":/*String*/ "JOSE JUAN LOPEZ"
									,"EQUICATGRY":/*String*/ "W"
									,"EQUIPMENT":/*String*/ "000000000013000152"
									,"EQUIP_LABEL":/*String*/ "WES-873"
									,"FLEET_NUM":/*String*/ "ID 98201"
									,"LICENSE_NUM":/*String*/ "WES-873"
									,"LOAD_VOL":/*int*/ 0
									,"MAINTPLANT":/*String*/ "D074"
									,"OBJNR":/*String*/ "IE000000000013000152"
									,"STATUS":/*String*/ "AVLB"
									,"STATUS_LIGHT":/*String*/ "N"
									,"TURN_DATE":/*Date*/ new Date(2011,7,08,0,0,0)
									,"TURN_TIME":/*String*/ "00:00:00"
									,"VEHICLE_TYPE":/*String*/ "930"
									,"VOL_UNIT":/*String*/ ""
								}
							])
							,"LAND":/*String*/ "MX"
							,"NAME":/*String*/ "MX-PD-063 CENTRAL"
							,"PLANT_ID":/*String*/ "D074"
							,"WORKCENTER":new ArrayCollection([
								/*sap.core.wd.context::WDContextNode*/{
									"ARBPL":/*String*/ "DOSIFI_1"
								}
								,/*sap.core.wd.context::WDContextNode*/{
									"ARBPL":/*String*/ "DOSIFI_A"
								}
							])
							,"ZCLOSE":/*Boolean*/ false
							,"ZOPERATIVE_MODE":/*String*/ "01"
						}
					])
					,"SAP_PLANT":/*String*/ ""
					,"USER":/*String*/ "E0USERDISP"
					,"leadselection":/*String*/ "0"
					,"name":/*String*/ "IMPORT_DISSO"
				}
				,"INITIALCONFIG":/*sap.core.wd.context::WDContextNode*/{
					"COLORMAP":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffffff"
							,"STATUS":/*String*/ "O_NORMAL"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffff00"
							,"STATUS":/*String*/ "O_RNGT"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffc000"
							,"STATUS":/*String*/ "O_DELA"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffffff"
							,"STATUS":/*String*/ "L_NORMAL"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffc000"
							,"STATUS":/*String*/ "L_DELA"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffffff,0x0070c0,3"
							,"STATUS":/*String*/ "L_PUMP"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xffff00"
							,"STATUS":/*String*/ "L_PROV"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0x548dd4"
							,"STATUS":/*String*/ "L_MAKEUP"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xd8d8d8"
							,"STATUS":/*String*/ "L_DIST"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0x7030a0"
							,"STATUS":/*String*/ "L_COLLECT"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0x92d050"
							,"STATUS":/*String*/ "L_BACHED"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xb6dde8"
							,"STATUS":/*String*/ "L_ONHOLD"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0x974706"
							,"STATUS":/*String*/ "L_DELIVER"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"RGB_STENCIL":/*String*/ "0xb6dde8,0xff0000,3"
							,"STATUS":/*String*/ "L_RESERVE"
						}
					])
					,"FX_OTR_LABELS":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ACTIVE_VEHICLES"
							,"VALUE":/*String*/ "Vehiculos Activos"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ADDITIONAL_COMMENTS"
							,"VALUE":/*String*/ "Comentarios Adicionales"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ALL_LINK"
							,"VALUE":/*String*/ "[ Todo ]"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ALL_LINK_TOOLTIP"
							,"VALUE":/*String*/ "Mostrar todas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_AMOUNT_TO_DISPATCH"
							,"VALUE":/*String*/ "Cantidad a Despachar"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED_LOADS_HEADER"
							,"VALUE":/*String*/ "Cargas Asignadas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED_LOADS_HEADER_TOOLTIP"
							,"VALUE":/*String*/ "Cargas Asignadas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED_VEHICLE"
							,"VALUE":/*String*/ "Vehiculo Asignado"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGNED_VOLUME"
							,"VALUE":/*String*/ "Volumen Asignado"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_ANTICIPATED_LOAD"
							,"VALUE":/*String*/ "Asignar carga anticipada"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_CHANGE_VEHICLE"
							,"VALUE":/*String*/ "Asignar Cambio Vehiculo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_PLANT_NO_VEHICLE"
							,"VALUE":/*String*/ "La carga no pueden ser enviada sin un vehículo. Por favor, asigne un vehículo y vuelva a intentarlo de nuevo."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_PLANT_WARNING"
							,"VALUE":/*String*/ "Asignar a la Planta. Advertencia!"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_TO_PLANT"
							,"VALUE":/*String*/ "Asignar a Planta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_VEHICLE"
							,"VALUE":/*String*/ "Asignar Vehiculo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ASSIGN_VEHICLE_HEADER"
							,"VALUE":/*String*/ "Asignar Vehiculo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_BATCH"
							,"VALUE":/*String*/ "Lote"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_BTN_CONFIRM"
							,"VALUE":/*String*/ "confirmar"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_BTN_IN_PLANT"
							,"VALUE":/*String*/ "En Planta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CANCEL"
							,"VALUE":/*String*/ "Cancelar"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CANCELED_TOOLTIP"
							,"VALUE":/*String*/ "Canceled"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_DELIVERY_TIME"
							,"VALUE":/*String*/ "Cambiar tiempo de entrega"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_DELIVERY_TIME_HEADER"
							,"VALUE":/*String*/ "Cambiar el tiempo de entrega"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_FREQUENCY"
							,"VALUE":/*String*/ "Change Frequency"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_PLANT"
							,"VALUE":/*String*/ "Cambiar Planta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_VOLUME"
							,"VALUE":/*String*/ "Cambiar Volumen"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_VOLUME_AFFECTED_HEADER"
							,"VALUE":/*String*/ "El volumen de la orden fué afectado"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHANGE_VOLUME_HEADER"
							,"VALUE":/*String*/ "Cambio de Volumen"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHECK_ALL_VEHICLES"
							,"VALUE":/*String*/ "Todos los Vehiculos"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CHECK_CLOSE_PLANT"
							,"VALUE":/*String*/ "Cerrar Planta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_COLD_JOINT_RISK_TITLE"
							,"VALUE":/*String*/ "Riesgo de Unión en Frio!"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CONFIRM_SHIPPING_DETAILS"
							,"VALUE":/*String*/ "Por favor, confirme los detalles de envío"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CONTACT"
							,"VALUE":/*String*/ "Contacto"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CONTINUE_PRODUCTION"
							,"VALUE":/*String*/ "Continuar la producción"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_COST_CURRENCY"
							,"VALUE":/*String*/ "$"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_COST_LABEL"
							,"VALUE":/*String*/ "Costo Actual"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_FREQUENCY"
							,"VALUE":/*String*/ "Frecuencia Actual"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_PLANT"
							,"VALUE":/*String*/ "Current Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CURRENT_TIME_TOOLTIP"
							,"VALUE":/*String*/ "Tiempo Actual"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_CUSTOMER"
							,"VALUE":/*String*/ "Cliente"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DELIVERED_AMOUNT"
							,"VALUE":/*String*/ "Cantidad Entregada"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DELIVERED_VOLUME"
							,"VALUE":/*String*/ "Volumen Entregado"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DELIVERY_TIME"
							,"VALUE":/*String*/ "Tiempo Entrega"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DISPATCH"
							,"VALUE":/*String*/ "Expedición"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DISPLAY_PO"
							,"VALUE":/*String*/ "Mostrar PO"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DISPOSE_LOAD"
							,"VALUE":/*String*/ "Desechar Carga"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_DRIVER"
							,"VALUE":/*String*/ "Conductor"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EARLIER_BATCHING"
							,"VALUE":/*String*/ "Earlier Batching"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EARLIEST_BATCHING"
							,"VALUE":/*String*/ "Primer Procesamiento por lotes"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_DELIVERY_CONFIRM_PUMPING"
							,"VALUE":/*String*/ "Confirmación de la entrega de servicios de bombeo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_MAIN_MENU"
							,"VALUE":/*String*/ "Fin de Día"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_OPERATIVE_MODE"
							,"VALUE":/*String*/ "Modo operativo de la Planta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_RVP_MSG_ASSIGNED_USER"
							,"VALUE":/*String*/ "lo esta atendiendo el usuario"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_RVP_MSG_WORKFLOW"
							,"VALUE":/*String*/ "el flujo de trabajo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_VEHICLE"
							,"VALUE":/*String*/ "Liberación de vehículo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_EOD_VEHICLE_PLANT"
							,"VALUE":/*String*/ "Liberación de vehículos en la planta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ESTIMATED_NEW_BATCHING_TIME"
							,"VALUE":/*String*/ "Nuevo tiempo estimado de procesamiento por lotes"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ESTIMATED_TIME_ON_HOLD"
							,"VALUE":/*String*/ "Tiempo estimado en espera"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_FXD_TEST"
							,"VALUE":/*String*/ "Prueba"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_IN_PROGRESS_VOLUME"
							,"VALUE":/*String*/ "Volumen en Progreso"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_JOB_SITE"
							,"VALUE":/*String*/ "Lugar de trabajo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LICENSE_PLATE"
							,"VALUE":/*String*/ "Licencia de Placas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD"
							,"VALUE":/*String*/ "Carga"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD2"
							,"VALUE":/*String*/ "Carga"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOADS_PER_ORDER_HEADER"
							,"VALUE":/*String*/ "Cargas por Órden"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOADS_PER_ORDER_HEADER_TOOLTIP"
							,"VALUE":/*String*/ "Cargas por Orden"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOADS_PER_VEHICLE_HEADER"
							,"VALUE":/*String*/ "Carga por Vehiculo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOADS_PER_VEHICLE_HEADER_TOOLTIP"
							,"VALUE":/*String*/ "Ctrl+Click para Mostrar Tod"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD_CONTAINS_CONTRUCTION_PRODUCT"
							,"VALUE":/*String*/ "La carga seleccionada contiene Productos de Construcción"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD_LABEL"
							,"VALUE":/*String*/ "Carga"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_LOAD_LEVEL"
							,"VALUE":/*String*/ "Load Level"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MANUAL_BATCH"
							,"VALUE":/*String*/ "Lote Manual"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MONITORING_GRAPH_MAIN_MENU"
							,"VALUE":/*String*/ "Grafica de Monitoreo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MONITORING_LOADS_PH_GRAPH_OPTION"
							,"VALUE":/*String*/ "Gráfica Cargas por Hora"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_MONITORING_VEHICLES_PH_GRAPH_OPTION"
							,"VALUE":/*String*/ "Gráfica de Vehículos por hora"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_DELIVERY_TIME"
							,"VALUE":/*String*/ "Nuevo Tiempo de entrega"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_FREQUENCY"
							,"VALUE":/*String*/ "Nueva Frecuencia"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_LOADING_TIME"
							,"VALUE":/*String*/ "Nuevo Tiempo de carga"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_LOAD_BATCHING_TIME"
							,"VALUE":/*String*/ "Nueva Hora para la carga de procesamiento por lotes"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEW_VOLUME"
							,"VALUE":/*String*/ "Nuevo Volumen"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NEXT_LOAD_BATCHING_TIME"
							,"VALUE":/*String*/ "Siguiente Tiempo de procesamiento por lotes de carga"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_NUMBER_VEHICLES_RELEASE"
							,"VALUE":/*String*/ "Number of Vehicles suggested for release"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_OPTIMAL_COST_CURRENCY"
							,"VALUE":/*String*/ "$"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_OPTIMAL_COST_LABEL"
							,"VALUE":/*String*/ "Costo Optimo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_AMOUNT"
							,"VALUE":/*String*/ "Cantidad del pedio"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_LABEL"
							,"VALUE":/*String*/ "Order"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_LEVEL"
							,"VALUE":/*String*/ "Order Level"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_NUMBER"
							,"VALUE":/*String*/ "Order"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_NUMBER2"
							,"VALUE":/*String*/ "Número de órden"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_ON_HOLD"
							,"VALUE":/*String*/ "Orden en espera"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_ON_HOLD_HEADER"
							,"VALUE":/*String*/ "Orden en espera"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_RENEGOTIATE_HEADER"
							,"VALUE":/*String*/ "Renegociar Orden"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_TAKING"
							,"VALUE":/*String*/ "Order Taking"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_TO_CONFIRM_ASK"
							,"VALUE":/*String*/ "Póngase en contacto con el cliente para confirmar los detalles."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_UNHOLD_CONFIRM"
							,"VALUE":/*String*/ "¿La orden ha sido confirmada?"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORDER_UNHOLD_DESC"
							,"VALUE":/*String*/ "La orden seleccionada pertenece a una orden retenida. Esto requiere confirmación por parte del cliente."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ORIGINAL_DELIVERY_TIME"
							,"VALUE":/*String*/ "Tiempo de entrega original"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PENDING_AMOUNT"
							,"VALUE":/*String*/ "Cantidad en espera"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PENDING_LOADS"
							,"VALUE":/*String*/ "Cargas Pendientes"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PENDING_VOLUME"
							,"VALUE":/*String*/ "Volumen Pendiente"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT"
							,"VALUE":/*String*/ "Planta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANTSELECTION_MAIN_MENU"
							,"VALUE":/*String*/ "Seleccion de Plantas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_ASSIGNED_TOOLTIP"
							,"VALUE":/*String*/ "Planta Asignada"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_CONFIRMED_TOOLTIP"
							,"VALUE":/*String*/ "Planta Confirmada"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_DELIVERED_TOOLTIP"
							,"VALUE":/*String*/ "Plant Delivered"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_GROUP"
							,"VALUE":/*String*/ "Grupo de Plantas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_GROUP_PLANTS_NAME"
							,"VALUE":/*String*/ "Por favor, especifique un nombre para este grupo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_OPERATIVE_MODE"
							,"VALUE":/*String*/ "Modo Operativo de Planta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_OP_MODE_AUTONOMOUS"
							,"VALUE":/*String*/ "Modo Autonomo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_OP_MODE_CENTRALIZED"
							,"VALUE":/*String*/ "Modo Centralizado"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_OP_MODE_DISTRIBUTED"
							,"VALUE":/*String*/ "Modo Distribuido"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_SCHEDULED_TOOLTIP"
							,"VALUE":/*String*/ "Planta Programada"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_SELECTION_HEADER"
							,"VALUE":/*String*/ "Selección de Plantas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PLANT_SELECT_PLANTS"
							,"VALUE":/*String*/ "Seleccione Plantas para formar un Grupo de Plantas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PRODUCT"
							,"VALUE":/*String*/ "Producto"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PRODUCTION_ORDER_TOOLTIP"
							,"VALUE":/*String*/ "OP"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PROVISIONAL_LOAD_DETAIL1"
							,"VALUE":/*String*/ "La carga seleccionada se encuentra en estado provisional. Se requiere confirmación por parte del cliente."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PROVISIONAL_LOAD_HEADER"
							,"VALUE":/*String*/ "Carga Provisional!"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PROVISIONAL_ORDER_DESC"
							,"VALUE":/*String*/ "La Orden que está intentando enviar no esta completa y requiere información adicional para ser enviada."
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PROVISIONAL_ORDER_HEADER"
							,"VALUE":/*String*/ "Orden Provisional"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_PUSH_BACK_TIME"
							,"VALUE":/*String*/ "Tiempo de retraso"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_QUALITY_ADJUSTMENT"
							,"VALUE":/*String*/ "Ajuste de calidad"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_REASON"
							,"VALUE":/*String*/ "Motivo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_REDIRECT_LOAD_OPTION"
							,"VALUE":/*String*/ "Redirect Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_REFRESH_TOOLTIP"
							,"VALUE":/*String*/ "Actualizar"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RELEASED_VEHICLES"
							,"VALUE":/*String*/ "Vehiculos Liberados"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RELEASE_ACTIVE_VEHICLES_HEADER"
							,"VALUE":/*String*/ "Liberación de los Vehículos Activos"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RELEASE_VEHICLE"
							,"VALUE":/*String*/ "Release Vehicle"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RENEGOTIATE"
							,"VALUE":/*String*/ "Renegociar"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RENEGOTIATION_LABEL"
							,"VALUE":/*String*/ "Renegotiate"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RESTART_LOAD"
							,"VALUE":/*String*/ "Reiniciar carga"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RETURNED_LOAD_MAIN_MENU"
							,"VALUE":/*String*/ "Carga devuelta"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RETURNED_LOAD_OPTION"
							,"VALUE":/*String*/ "Returned Load"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RE_PRINT"
							,"VALUE":/*String*/ "Reimpresión"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_RE_USE"
							,"VALUE":/*String*/ "Reuso"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SCHEDULED_VOLUME"
							,"VALUE":/*String*/ "Volumen programado"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SHOW_ALL_TOOLTIP"
							,"VALUE":/*String*/ "Mostrar Todas"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SIMULATE"
							,"VALUE":/*String*/ "Simular"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_START_BATCHING"
							,"VALUE":/*String*/ "Inicio despachar"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_STATUS"
							,"VALUE":/*String*/ "Estatus"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_SUPPORT_PLANT"
							,"VALUE":/*String*/ "Support Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TELEPHONE_SHORT"
							,"VALUE":/*String*/ "Tel"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TIME_ON_HOLD"
							,"VALUE":/*String*/ "Tiempo de entrega"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_TO_BE_CONFIRMED_TOOLTIP"
							,"VALUE":/*String*/ "por confirmar"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_UNASSIGN_TO_PLANT"
							,"VALUE":/*String*/ "Unassigned to plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE"
							,"VALUE":/*String*/ "Vehículo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_CONFIRMED_TOOLTIP"
							,"VALUE":/*String*/ "Vehículo confirmado"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_DELIVERED_TOOLTIP"
							,"VALUE":/*String*/ "Vehicle delivered"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_ID"
							,"VALUE":/*String*/ "Id. Vehículo"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_IN_PLANT"
							,"VALUE":/*String*/ "Vehicle in Plant"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VEHICLE_TO_BE_CONFIRMED_TOOLTIP"
							,"VALUE":/*String*/ "Vehicle to be confirmed"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VH_IN_PLANT"
							,"VALUE":/*String*/ "Registro de Vehículos"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VISUALIZATION_MAIN_MENU"
							,"VALUE":/*String*/ "Visualización"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_VOLUME"
							,"VALUE":/*String*/ "Volumen"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_WARNING"
							,"VALUE":/*String*/ "Advertencia"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ZOOM_IN_TOOLTIP"
							,"VALUE":/*String*/ "Acercar"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"ALIAS":/*String*/ "ZMXCSDSLS/DISP_ZOOM_OUT_TOOLTIP"
							,"VALUE":/*String*/ "Alejar"
						}
					])
					,"FX_PARAMETERS":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "ASSI_VEHI_NOT_PL"
							,"VALUE":/*String*/ "5"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "CHANGE_ASSI_VEHI"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "CHANGE_VOL_CASH"
							,"VALUE":/*String*/ "120"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "COLLECT_LOAD_DIS"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "CONSTRUC_PRO_DIS"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "COST_REFRESH_TIM"
							,"VALUE":/*String*/ "11"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "DISP_ID_OR_LICEN"
							,"VALUE":/*String*/ "LP"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "DISP_PUMP_DFE"
							,"VALUE":/*String*/ "5"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "LIFE_SPAN"
							,"VALUE":/*String*/ "1"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "MAX_DISP_DELIVER"
							,"VALUE":/*String*/ "240"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "MAX_DISP_FUTURE"
							,"VALUE":/*String*/ "120"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "MAX_TIME_ANTICIP"
							,"VALUE":/*String*/ "<NO_VALUE>"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "MAX_TIME_ASSI_VE"
							,"VALUE":/*String*/ "15"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "NUM_DAY_NON_AP_T"
							,"VALUE":/*String*/ "2"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "NUM_OPC_DISP_MEP"
							,"VALUE":/*String*/ "5"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "NUM_VEHI_DISP"
							,"VALUE":/*String*/ "5"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "PLANT_VIS_AREA_L"
							,"VALUE":/*String*/ "180"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "PUNCTUAL_MEASURE"
							,"VALUE":/*String*/ "2"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "REFERENCE_PUNTU"
							,"VALUE":/*String*/ "2"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "TIME_LOAD_DELAY"
							,"VALUE":/*String*/ "59"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "TIME_REDIRECT"
							,"VALUE":/*String*/ "120"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "TIME_REFRESH_DFE"
							,"VALUE":/*String*/ "120"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "TIME_SLOTS_DFE"
							,"VALUE":/*String*/ "5"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"NAME":/*String*/ "VEHIC_MAX_CAPACI"
							,"VALUE":/*String*/ "8"
						}
					])
					,"FX_TVARVC":new ArrayCollection([
						/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ADBO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ADDITION"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "AVLB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ALL_EQUI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ALL_EQUI"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ALL_EQUI"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ASSG"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ASSN"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "AVLB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_AVLB"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CMPL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CNCL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "01"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLL_ORDE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XF26966"
							,"LOW":/*String*/ "19"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "18"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XDBDBDB"
							,"LOW":/*String*/ "20"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "19"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XF9E4C9"
							,"LOW":/*String*/ "21"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "20"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XD0E0EB"
							,"LOW":/*String*/ "22"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "21"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XE4DCE2"
							,"LOW":/*String*/ "23"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "22"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XEDDAF0"
							,"LOW":/*String*/ "24"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "23"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC7E9EE"
							,"LOW":/*String*/ "25"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "24"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XCCE4E4"
							,"LOW":/*String*/ "26"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "25"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XEFEFAF"
							,"LOW":/*String*/ "27"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "26"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XE5E0C2"
							,"LOW":/*String*/ "28"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "27"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XE8E9DB"
							,"LOW":/*String*/ "29"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "28"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XCDEBAB"
							,"LOW":/*String*/ "30"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "29"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFDBA73"
							,"LOW":/*String*/ "31"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "30"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFFFF"
							,"LOW":/*String*/ "32"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "31"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFEF745"
							,"LOW":/*String*/ "33"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "32"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFC8A"
							,"LOW":/*String*/ "34"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "33"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFFDBD"
							,"LOW":/*String*/ "35"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "34"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC2DFA9"
							,"LOW":/*String*/ "18"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "17"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFF645D"
							,"LOW":/*String*/ "1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XBEEC9C"
							,"LOW":/*String*/ "2"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFD675C"
							,"LOW":/*String*/ "3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFF7A71"
							,"LOW":/*String*/ "4"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFF978E"
							,"LOW":/*String*/ "5"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFAAA38"
							,"LOW":/*String*/ "6"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFFB671"
							,"LOW":/*String*/ "7"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XFCCE8B"
							,"LOW":/*String*/ "8"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0X95D78F"
							,"LOW":/*String*/ "9"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XAAEEA3"
							,"LOW":/*String*/ "10"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC4F9C4"
							,"LOW":/*String*/ "11"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "10"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XA1E5FF"
							,"LOW":/*String*/ "12"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "11"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XB7CEEE"
							,"LOW":/*String*/ "13"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "12"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XC1D7F2"
							,"LOW":/*String*/ "14"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "13"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XD5E7F4"
							,"LOW":/*String*/ "15"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "14"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XBADDDF"
							,"LOW":/*String*/ "16"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "15"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "0XF5F67E"
							,"LOW":/*String*/ "17"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_COLOR_CAT"
							,"NUMB":/*String*/ "16"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRETE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLOCKED"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONCRET_L"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CONF"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CP ONLY"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTNR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CPTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTNR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_CPTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "12/3|12/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_ADVANCE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "33/3|33/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_BLOCKED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "12/12"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_CALL_BK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "3/3|3/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_CANCELL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "9/9|9/9"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_COMPLET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "6/6|6/6"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_COMPL_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "32/0|32/0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_CONFIRM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "8/8|8/8"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_DELAYED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "11/11|11/11"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_IN_PROC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "11/3|32/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_ORDER_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "22/22"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_PUMP_SE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "33/33|33/3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_RENEGOT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "31/31"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "12/0|12/12"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_TO_BE_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "20/20"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_C_VISI_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DELIV_VOL"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DELIV_VOL"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DELIV_VOL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DELIV_VOL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TCBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_DLY_PLL_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "V"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_EQTYP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_ASL_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_ASL_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPO_O"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_LPV_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "MAX_TIME_ASSI_VE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "12"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "NUM_DAY_NON_AP_T"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "13"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "NUM_OPC_DISP_MEP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "14"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "NUM_VEHI_DISP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "15"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "PLANT_VIS_AREA_L"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "16"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "PUNCTUAL_MEASURE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "17"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "REFERENCE_PUNTU"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "18"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "TIME_LOAD_DELAY"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "19"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "TIME_REDIRECT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "20"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "TIME_REFRESH_DFE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "21"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "TIME_SLOTS_DFE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "22"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "VEHIC_MAX_CAPACI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "23"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "ASSI_VEHI_NOT_PL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " , , ,X"
							,"LOW":/*String*/ "CHANGE_ASSI_VEHI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "CHANGE_VOL_CASH"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "COLLECT_LOAD_DIS"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "CONSTRUC_PRO_DIS"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "COST_REFRESH_TIM"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "DISP_ID_OR_LICEN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "DISP_PUMP_DFE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "LIFE_SPAN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "MAX_DISP_DELIVER"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "X, , ,"
							,"LOW":/*String*/ "MAX_DISP_FUTURE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "10"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ " ,X, ,"
							,"LOW":/*String*/ "MAX_TIME_ANTICIP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_FXD_PARAM"
							,"NUMB":/*String*/ "11"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "Y"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GPS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GPS_INC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "Z001"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GRULG"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ATPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "10"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ARRV"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "9"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNS"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSN"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "AVLB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_GV_VEHI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "999"
							,"LOW":/*String*/ "950"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HOLD_REAS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_HOLI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_INPC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_ADVANCE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_ADVANCE"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_BLOCKED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_CALL_BK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_CALL_BK"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_COMPLET"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_COMPLET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_COMPL_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_COMPL_D"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_CONFIRM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_CONFIRM"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_DELAYED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_DELAYED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_IN_PROC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_IN_PROC"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_ORDER_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_ORDER_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_PUMP_SE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_RENEGOT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_SELF_CO"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_TO_BE_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_TO_BE_C"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_VISI_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_I_VISI_PL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ID"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LABEL_ID"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LP"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LABEL_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LDNG"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OH"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LIFSK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LOAD_SCOPE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_LOAD_SCOP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_ADVANCE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_ADVANCE"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0x92d050"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_BACHED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_BLOCKED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_BLOCKED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_CALL_BK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_CANCELL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0x7030a0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_COLLECT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_COMPLET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_CONFIRM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffc000"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DELA"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DELAYED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DELAYED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0x974706"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DELIVER"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xd8d8d8"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_DIST"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_IN_PROC"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0x548dd4"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_MAKEUP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffffff"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_NORMAL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xb6dde8"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_ONHOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_ORDER_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffff00"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_PROV"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffffff,0x0070c0,3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_PUMP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_PUMP_SE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0X00FF33"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_QUEUED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_RENEGOT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_RENEGOT"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_RENEGOT"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xb6dde8,0xff0000,3"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_RESERVE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_SELF_CO"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_STARTED"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_STARTED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_STARTED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0XFF0000"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_STOPPED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_TO_BE_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_L_VISI_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTAT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_MAINTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "N"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NOGPS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_NOST"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ORDER_SCOPE"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ORDE_SCOP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_ADVANCE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLCK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_BLOCKED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CALL_BK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CALL_BK"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CALL_BK"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CANCELL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_COMPLET"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_COMPL_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_CONFIRM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffc000"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_DELA"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_DELAYED"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_DELAYED"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_IN_PROC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffffff"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_NORMAL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_ORDER_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_PUMP_SE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_RENEGOT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "0xffff00"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_RNGT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_SELF_CO"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_TO_BE_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_O_VISI_PL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZCOC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PAYTERM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZCOD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PAYTERM"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZV"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PF_DCEMEX"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "03"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PLOPM_A"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "01"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PLOPM_C"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "02"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PLOPM_D"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ASSGND"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ASSGND"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGPLN"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGPLN"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGVEH"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGVEH"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_CHGVEH"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_DLVTIM"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_DLVTIM"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_GNERAL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_GNERAL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHOLD"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHOLD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PL_ONHOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZREADYMX"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PP_PROFI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTC1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTNR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROD"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZREADMX1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PROFILE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ASSGND"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PR_ONHOLD"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_ASS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CNCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CNC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CON"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_CON"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_DEL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PTT_L_TBC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZTX0"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_PUMP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_L"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_L"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_L"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_O"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_O"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENEGOT_O"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ "949"
							,"LOW":/*String*/ "900"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RENG_REAS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "BT"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_RNGT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRMS0001"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SD_PROFI"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "02"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHIPCOND"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM1"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHTYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZRM2"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHTYPE"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "Z005"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_SHTYPE_PK"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "E0008"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STAT"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "8"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLI"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ASSG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "NOST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATLD"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "RNGT"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "7"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "HOLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "6"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ADBO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "5"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "BLCK"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "INPC"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCO"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATOR"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "20"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_STATPL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "E0001"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_ST_CREATE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TBCL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZCI4"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TEXT_ID"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TJST"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TOPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_TOPL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "VAA"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VAA"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "VA-M-P"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VAMP"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "VAM-S"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VAMS"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VATYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ ""
							,"SIGN":/*String*/ ""
							,"TYPE":/*String*/ "P"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "ZADR"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VATYPE"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CONF"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_CON"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "LDNG"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_DEL"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TJST"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_DEL"
							,"NUMB":/*String*/ "1"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "OJOB"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_DEL"
							,"NUMB":/*String*/ "2"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "UNLD"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_DEL"
							,"NUMB":/*String*/ "3"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "CMPL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_DEL"
							,"NUMB":/*String*/ "4"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "TBCL"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_VTT_L_TBC"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
						,/*sap.core.wd.context::WDContextNode*/{
							"CLIE_INDEP":/*String*/ ""
							,"HIGH":/*String*/ ""
							,"LOW":/*String*/ "02"
							,"NAME":/*String*/ "Z_CXFM_CSDSLSMX_1005_X_SELF_CO"
							,"NUMB":/*String*/ "0"
							,"OPTI":/*String*/ "EQ"
							,"SIGN":/*String*/ "I"
							,"TYPE":/*String*/ "S"
						}
					])
					,"LCDS":/*sap.core.wd.context::WDContextNode*/{
						"LCDS_DESTINY":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"DESTINY_ID":/*String*/ "PLANT"
								,"DESTINY_NAME":/*String*/ "PlantPI"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"DESTINY_ID":/*String*/ "PRODUCTION_STATUS"
								,"DESTINY_NAME":/*String*/ "ProductionStatusPI"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"DESTINY_ID":/*String*/ "SALES_ORDER"
								,"DESTINY_NAME":/*String*/ "SalesOrderPI"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"DESTINY_ID":/*String*/ "VEHICLE"
								,"DESTINY_NAME":/*String*/ "VehiclePI"
							}
						])
						,"LCDS_ENDPOINT":/*sap.core.wd.context::WDContextNode*/{
							"CHANNEL_NAME":/*String*/ "weborb-rtmp-messaging"
							,"CHANNEL_TYPE":/*String*/ "weborb"
							,"URL":/*String*/ "rtmp://mxoccrmsrid01.noam.cemexnet.com:1951/weborb"
							,"leadselection":/*String*/ "0"
							,"name":/*String*/ "LCDS_ENDPOINT"
						}
						,"leadselection":/*String*/ "0"
						,"name":/*String*/ "LCDS"
					}
					,"PREFIXES":/*sap.core.wd.context::WDContextNode*/{
						"COLORMAP_PREFIX":/*String*/ ""
						,"FX_OTR_PREFIX":/*String*/ "ZMXCSDSLS/"
						,"FX_PARAM_PREFIX":/*String*/ ""
						,"FX_SECURITY_PREFIX":/*String*/ "DM_DISP_"
						,"FX_TVARVC_PREFIX":/*String*/ "Z_CXFM_CSDSLSMX_1005_"
						,"leadselection":/*String*/ "0"
						,"name":/*String*/ "PREFIXES"
					}
					,"SECURITY":/*sap.core.wd.context::WDContextNode*/{
						"EXEPTIONS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "NNN"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_SUPPORT_PLANT"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_RELEASE_VEHICLE"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_VEHICLE_IN_PLANT"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RENEGOTIATE_LOAD"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"APLICATION":/*String*/ "DISP"
								,"ATTRIBUTE_NAME":/*String*/ "BTN_UNASSIGN_PLANT"
								,"CONFIGURATION":/*String*/ "YYY"
								,"LAND":/*String*/ "MX"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"ROL":/*String*/ "DISP"
							}
						])
						,"FIELDS_PER_PROCESS":new ArrayCollection([
							/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,6,28,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIG_TO_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "20:53:24"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_SUPPORT_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_RELEASE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_VEHICLE_IN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RENEGOTIATE_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_UNASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_DISP_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD_PRODUCTION"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_DISPLAY_PO"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ false
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_DISPATCH"
								,"ENABLED":/*Boolean*/ false
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_BATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_MANUAL_BATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RE_PRINT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RE_USE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CONTINUE_PRODUCTION"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RESTART_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_QUALITY_ADJUSTMENT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_ORDER"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD_PRODUCTION"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_UNASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "DRG_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_ONE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_VOLUME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_ORDER_ON_HOLD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_DELIVERY_TIME"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_DISPLAY_PO"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_DISPATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_BATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_MANUAL_BATCH"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RE_PRINT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RE_USE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CONTINUE_PRODUCTION"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RESTART_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_QUALITY_ADJUSTMENT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_FREQUENCY"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_SUPPORT_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_RELEASE_VEHICLE"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "VEHICLE_VEHICLE_IN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_LOADS_PER_VEHICLE"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "TIP_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_CHANGE_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_SHOW_DATA"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_START_BATCHING"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_RENEGOTIATE_LOAD"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
							,/*sap.core.wd.context::WDContextNode*/{
								"AEDTM":/*Date*/ new Date(2011,7,01,0,0,0)
								,"ATTRIBUTE_NAME":/*String*/ "BTN_UNASSIGN_PLANT"
								,"ENABLED":/*Boolean*/ true
								,"LAND":/*String*/ "MX"
								,"MODOEN":/*String*/ "1"
								,"PROCESS":/*String*/ "DM_BATC_ASSIGNED_LOADS"
								,"PSOTM":/*String*/ "19:27:49"
								,"READ_ONLY":/*Boolean*/ false
								,"REQUIRED":/*Boolean*/ true
								,"UNAME":/*String*/ "E0HDRESTRP"
								,"VALDEFAULT":/*String*/ ""
								,"VISIBLE":/*Boolean*/ true
							}
						])
						,"leadselection":/*String*/ "0"
						,"name":/*String*/ "SECURITY"
					}
					,"USER_ROLES":/*sap.core.wd.context::WDContextNode*/{
						"IS_AGENT_SERVICE":/*Boolean*/ false
						,"IS_BATCHER":/*Boolean*/ false
						,"IS_DISPATCHER":/*Boolean*/ true
						,"leadselection":/*String*/ "0"
						,"name":/*String*/ "USER_ROLES"
					}
					,"leadselection":/*String*/ "0"
					,"name":/*String*/ "INITIALCONFIG"
				}
			}			
		;
	}
}