package com.cemex.rms.dispatcher.services.flashislands.events
{
	import flash.events.Event;
	
	public class FlashIslandUnfreezedEvent extends Event
	{
		
		public static const FLASHISLAND_UNFREEZED_ACKNOWLEDGE:String = "flashIslandUnfreezedAck";
		public function FlashIslandUnfreezedEvent(bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(FLASHISLAND_UNFREEZED_ACKNOWLEDGE, bubbles, cancelable);
		}
	}
}