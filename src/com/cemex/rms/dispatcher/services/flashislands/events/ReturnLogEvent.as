package com.cemex.rms.dispatcher.services.flashislands.events
{
	import com.cemex.rms.dispatcher.services.flashislands.vo.FlashIslandsData;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	public class ReturnLogEvent extends Event
	{
		public static const RETURN_LOG_EVENT:String = "returnLogEvent";
		
		public var returnLog:ArrayCollection;
		
		public function ReturnLogEvent(returnLog:ArrayCollection, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(RETURN_LOG_EVENT, bubbles, cancelable);
			this.returnLog = returnLog;
		}
	}
}