package com.cemex.rms.dispatcher.services.flashislands.vo
{
	import mx.collections.ArrayCollection;

	public class Plant
	{
		public function Plant()
		{
		}
		public var plantId:String;
		public var name:String;
		public var zclose:Boolean;
		//ZOPERATIVE_MODE
		public var zoperativeMode:String;
		public var land:String;
		
		public var equipments:ArrayCollection;
		public var workcenter:ArrayCollection;
		public var status:String;
	}
}