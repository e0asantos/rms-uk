package com.cemex.rms.dispatcher.services.flashislands.vo
{
	public class DateText
	{
		public function DateText()
		{
		}
		
		public var refDate:String;
		public var shortDate:String;
		public var largeDate:String;
	}
}