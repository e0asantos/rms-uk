package com.cemex.rms.dispatcher.services.flashislands.vo
{
	public class ProductionData
	{
		public function ProductionData()
		{
		}
		public var vbeln:String;
		public var txt04:String;
		public var aufnr:String;
		public var posnr:Number;
		public var stonr:String;
	}
}