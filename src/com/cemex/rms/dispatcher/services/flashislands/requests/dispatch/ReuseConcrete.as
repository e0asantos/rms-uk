package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ReuseConcrete implements IFlashIslandEventObject
	{
		public function ReuseConcrete()
		{
		}
		public var orderNumber:String;
		public var itemNumber:String;
		public var werks:String;
	
		
		public function getEventName():String{
			return "reuseConcrete";
		}
	}
}