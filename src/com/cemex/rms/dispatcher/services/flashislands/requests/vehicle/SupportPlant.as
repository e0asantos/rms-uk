 	package com.cemex.rms.dispatcher.services.flashislands.requests.vehicle
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class SupportPlant implements IFlashIslandEventObject
	{
		public function SupportPlant()
		{
		}
		
		public var EQUNR:String;
		public var WERKS:String;
	
		public function getEventName():String{
			return "supportPlant";
		}
	}
}