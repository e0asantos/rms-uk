 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class VisualConfigurationEvent implements IFlashIslandEventObject
	{
		public function VisualConfigurationEvent()
		{
		}
		public var showOnePlantAtATime:Boolean=false;
		public var availableVehiclesButton:Number=0;
		public var sortVehiclesPerTime:int=0;
		public var rangeVisible:Number=1;
		public var selectedPlant:String="LPO";
		public var sortVehicleFromOldestToNewest:Boolean=true;
		
		public var firstRow:String="ASSG";
		public var secondRow:String="AVLB";
		public var thirdRow:String="LOAD";
		public var plantConfigured:String=null;
		public var lastPlant:String="last";
		public var selectedView:String="LPO";
		
		
		public function getEventName():String{
			return "userPreferences";
		}
	}com.cemex.rms.common.flashislands.IFlashIslandEventObject;
}