 	package com.cemex.rms.dispatcher.services.flashislands.requests.vehicle
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ForceUnassign implements IFlashIslandEventObject
	{
		public function ForceUnassign()
		{
		}
		
		public var EQUNR:String;
		public var POSNR:String;
		public var ORDR:String
		public function getEventName():String{
			return "forceUnassign";
		}
	}
}