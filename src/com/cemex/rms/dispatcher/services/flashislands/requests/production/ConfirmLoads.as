 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ConfirmLoads implements IFlashIslandEventObject
	{
		public function ConfirmLoads()
		{
		}
		
		public var VBELN:String;
		public var POSNR:String;
		public var etenr:String;
		public var loadNumber:String;
		public var confirmPart:String;
		
			
		public function getEventName():String{
			return "confirmProduct";
		}
	}
}