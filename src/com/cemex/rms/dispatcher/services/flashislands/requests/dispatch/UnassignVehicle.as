package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class UnassignVehicle implements IFlashIslandEventObject
	{
		public function UnassignVehicle()
		{
		}	
		public var orderNumber:String;
		public var itemNumber:String;
		/*public var equipNumber:String;*/
		public var toPlant:String;
		public var loadNumber:String;
		public var toPlantType:String;
		public var loadStatus:String;
		public var piEquipment:String=" ";
		public var etenr:String;
		
		public function getEventName():String{
			return "unassignVehicle";
		}
	}
}