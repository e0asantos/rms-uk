 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class Bitacora implements IFlashIslandEventObject
	{
		public function Bitacora()
		{
		}
		public var orderNumber:String;
		//public var itemNumber:String;
		
		public function getEventName():String{
			return "showBitacora";
		}
	}
}