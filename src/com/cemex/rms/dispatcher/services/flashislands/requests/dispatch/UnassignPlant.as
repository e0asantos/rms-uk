 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class UnassignPlant implements IFlashIslandEventObject
	{
		public function UnassignPlant()
		{
		}
		public var orderNumber:String;
		public var itemNumber:String;
		public var toPlant:String;
		public var etenr:String;
		public var loadNumber:String;
		public var toPlantType:String;
		
		public function getEventName():String{
			return "unassignPlant";
		}
	}
}