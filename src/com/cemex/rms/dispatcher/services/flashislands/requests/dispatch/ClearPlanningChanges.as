 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ClearPlanningChanges implements IFlashIslandEventObject
	{			
		public function ClearPlanningChanges()
		{
		}
		
		public var plant:String;
		public function getEventName():String{
			return "clearPlanningChanges";
		}
	}
}