package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	
	public class CostData implements IFlashIslandEventObject
	{
		public function CostData()
		{
		}
		
		
		public var cost1:String;
		public var cost2:String;
		
		public function getEventName():String
		{
			return "costData";
		}
	}
}