 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ContinueProduction implements IFlashIslandEventObject
	{
		public function ContinueProduction()
		{
		}
		
		public var etenr:String;
		public var loadNumber:String;
		public var AUFNR:String;
		public var WERKS:String;
		public var VBELN:String;
		public var POSNR:String;
		public var EQUNR:String;
		public var LICENSE_NUM:String;
		public var MATNR:String;
		public var GAMNG:String;
		
		public function getEventName():String{
			return "continueProduction";
		}
	}
}