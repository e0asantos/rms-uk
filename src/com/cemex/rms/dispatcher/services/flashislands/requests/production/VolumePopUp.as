 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class VolumePopUp implements IFlashIslandEventObject
	{
		public function VolumePopUp()
		{
		}
		public var etenr:String;
		public var loadNumber:String;
		public var vbeln:String;
		public var posnr:String;
			
		public function getEventName():String{
			return "quickVolumeChange";
		}
	}
}