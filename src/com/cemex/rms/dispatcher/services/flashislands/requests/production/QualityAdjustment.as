 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class QualityAdjustment implements IFlashIslandEventObject
	{
		public function QualityAdjustment()
		{
		}
		public var AUFNR:String;
		public var STONR:String;
		
		public function getEventName():String{
			
			return "qualityAdjustment";
		}
	}
}