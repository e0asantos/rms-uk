package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class SearchOrderVehicleRequest implements IFlashIslandEventObject
	{
		public function SearchOrderVehicleRequest()
		{
		}
		
		public var view:String;
		
		
		public function getEventName():String{
			return "searchOrderVehicle";
		}
	}
}