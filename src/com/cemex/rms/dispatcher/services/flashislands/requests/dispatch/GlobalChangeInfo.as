 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class GlobalChangeInfo implements IFlashIslandEventObject
	{
		public function GlobalChangeInfo()
		{
		}
		public var orderNumber:String;
		public var eventType:String;
		//public var itemNumber:String;
		
		public function getEventName():String{
			return "globalChangeInfo";
		}
	}
}