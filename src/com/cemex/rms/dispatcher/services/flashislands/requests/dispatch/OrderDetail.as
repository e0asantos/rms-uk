package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	
	public class OrderDetail implements IFlashIslandEventObject
	{
		public function OrderDetail()
		{
		}
		
		
		public var orderNumber:String;
		public var posy:String;
		
		public function getEventName():String
		{
			return "orderDetail";
		}
	}
}