 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class BomRequest implements IFlashIslandEventObject
	{
		public function BomRequest()
		{
		}
		
		public var VBELN:String;
		public var MATNR:String;
		public var WERKS:String;
		public var BOM:String;
			
		public function getEventName():String{
			return "requestBom";
		}
	}
}