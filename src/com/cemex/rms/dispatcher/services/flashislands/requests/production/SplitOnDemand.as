 	package com.cemex.rms.dispatcher.services.flashislands.requests.production
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class SplitOnDemand implements IFlashIslandEventObject
	{
		public function SplitOnDemand()
		{
		}
		public var country:String;
		public var loadingDate:String;
		public var orderNumber:String;
		public function getEventName():String{
			return "splitOnDemand";
		}
	}
}