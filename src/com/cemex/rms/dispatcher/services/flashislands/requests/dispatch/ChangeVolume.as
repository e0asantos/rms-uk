package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ChangeVolume implements IFlashIslandEventObject
	{
		public function ChangeVolume()
		{
		}
		public var orderNumber:String;
		public var etenr:String;
		public var loadNumber:String;
		public var itemNumber:String;
		public var loadStatus:String;
		public function getEventName():String{
			return "changeVolume";
		}
	}
}