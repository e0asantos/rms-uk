 	package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class SetPlanningPlant implements IFlashIslandEventObject
	{
		
		public var plant:String;
		public function SetPlanningPlant()
		{
		}
		
		
		public function getEventName():String{
			return "planningPlant";
		}
	}
}