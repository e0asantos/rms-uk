package com.cemex.rms.dispatcher.services
{
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.dispatcher.services.flashislands.IDispatcherIsland;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherProducer;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherReceiver;
	
	import mx.collections.ArrayCollection;

	public interface IGanttServiceFactory
	{
		function getModel():IServiceModel;
		function getFlashIslandService():IDispatcherIsland;
		
		function getVehiclePIReceiver():IDispatcherReceiver ;
		function getSalesOrderPIReceiver():IDispatcherReceiver ;
		function getProductionStatusPIReceiver():IDispatcherReceiver ;
		function getPlantPIReceiver():IDispatcherReceiver ;
		
	}
}