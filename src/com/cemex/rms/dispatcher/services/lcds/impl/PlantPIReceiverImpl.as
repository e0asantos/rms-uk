package com.cemex.rms.dispatcher.services.lcds.impl
{
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherReceiver;
	import com.cemex.rms.dispatcher.services.lcds.events.PlantOperativeModeLCDSEvent;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	
	import mx.messaging.ChannelSet;
	
	public class PlantPIReceiverImpl 
		extends AbstractReceiverService
		implements IDispatcherReceiver {
		public function PlantPIReceiverImpl(){
			super();
		}
		
		public override function getDestinationName():String{
			return PushConfigHelper.getDestinyName("PLANT");
		}
		
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSet") as ChannelSet;
		}
		
		protected override function receiveMessage(message:Object):void {
		
			if (message != null && message.hasOwnProperty("plant")
				&& message.hasOwnProperty("operativeMode")){
				
				var plantId:String = message["plant"];
				var plantType:String = message["operativeMode"];
				var statusCode:String=message["PISTATUSCOD"];
				logger.info(ReflectionHelper.object2XML(message,"PlantOperativeModeLCDSEvent"));
				var event:PlantOperativeModeLCDSEvent =  new PlantOperativeModeLCDSEvent(plantId,plantType);
				event.statusCode=statusCode;
				dispatcher.dispatchEvent(event);
				
				
			}
			logger.info(ReflectionHelper.object2XML(message,getDestinationName()));
		}
	}
}