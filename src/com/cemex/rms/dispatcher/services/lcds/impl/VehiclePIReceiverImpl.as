package com.cemex.rms.dispatcher.services.lcds.impl
{
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.dispatcher.services.flashislands.requests.production.Dispatch;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherReceiver;
	import com.cemex.rms.dispatcher.services.lcds.events.VehicleLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.helpers.DispatchLCDSDynamicObjectHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class VehiclePIReceiverImpl 
		extends AbstractReceiverService 
		implements IDispatcherReceiver {
		public function VehiclePIReceiverImpl(){
			super();
		}
		
		public override function getDestinationName():String{
			return PushConfigHelper.getDestinyName("VEHICLE");
		}
		
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSet") as ChannelSet;
		}
		
		protected override function receiveIMessage(message:IMessage):void {
			logger.info(ReflectionHelper.object2XML(message,getDestinationName()));
			super.receiveIMessage(message);
		}
		protected override function receiveMessage(message:Object):void {
			
			logger.info(ReflectionHelper.object2XML(message,getDestinationName()));
			if (message != null && message["equiCatgry"] != null ){
				
				//logger.info(ReflectionHelper.object2XML(message,"VehicleLCDSEvent"));
				
				var equipment:Equipment = DispatchLCDSDynamicObjectHelper.transformVehicle(message);
				var event:VehicleLCDSEvent =  new VehicleLCDSEvent(equipment);
				dispatcher.dispatchEvent(event);
				
			}
			
		}
	}
}