package com.cemex.rms.dispatcher.services.lcds.impl
{
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.AbstractService;
	import com.cemex.rms.dispatcher.services.flashislands.vo.AddProd;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherReceiver;
	import com.cemex.rms.dispatcher.services.lcds.events.ProductionStatusLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.events.SalesOrderLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.helpers.DispatchLCDSDynamicObjectHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class ProductionStatusPIReceiverImpl extends AbstractReceiverService implements IDispatcherReceiver
	{
		public function ProductionStatusPIReceiverImpl()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return PushConfigHelper.getDestinyName("PRODUCTION_STATUS");
		}
		
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSet") as ChannelSet;
		}
		
		protected override function receiveMessage(message:Object):void {
			if (message != null && message.hasOwnProperty("VBELN") ){
				

				var productionData:ProductionData = DispatchLCDSDynamicObjectHelper.transformProductionData(message);
				
				var event:ProductionStatusLCDSEvent =  new ProductionStatusLCDSEvent(productionData);
				dispatcher.dispatchEvent(event);
				
			}
			logger.info(ReflectionHelper.object2XML(message,getDestinationName()));
		}
		
		
		
		
	}
}