package com.cemex.rms.dispatcher.services.lcds.impl
{
	import com.cemex.rms.common.push.AbstractChannelSetService;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.ChannelSet;

	public class DispatcherChannelSetImpl extends AbstractChannelSetService
	{
		public function DispatcherChannelSetImpl()
		{
			super();
		}
		
		protected override function setChannelSet(channelSet:ChannelSet):void{
			
			servicesContext.setValue("ChannelSet",channelSet);
		} 
	
	}
}