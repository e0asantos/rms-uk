package com.cemex.rms.dispatcher.services.lcds.mock
{
	import com.cemex.rms.common.services.AbstractService;
	import com.cemex.rms.dispatcher.events.deltas.DeltaLoadEvent;
	import com.cemex.rms.dispatcher.gantt.vo.GanttTask;
	import com.cemex.rms.dispatcher.services.lcds.IDispatcherLCDS;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	public class DispatcherLCDSMock extends AbstractService implements IDispatcherLCDS
	{
		public function DispatcherLCDSMock() {
			super();	
		}
		public override function startService():void {
			dispatchServiceLoadReady();
			dispatcher.addEventListener(DeltaLoadEvent.ADD_DELTA_LOAD, addDeltaLoad );
		}
		
		private var queue:ArrayCollection =  new ArrayCollection();
		public function addDeltaLoad(e:DeltaLoadEvent):void {
			if (e.step == GanttTask.STEP_GHOSTX){
				
				var event:DeltaLoadEvent =  new DeltaLoadEvent(e.type, e.payload , GanttTask.STEP_MOVED );
					
				queue.addItem(event);
				var timer:Timer = new Timer(5*1000,1);
				timer.addEventListener(TimerEvent.TIMER,lanzaEvento);
				timer.start();
				
			}
		}
		
		
		public function lanzaEvento(e:TimerEvent):void{
			if (queue.length > 0){
				var event:DeltaLoadEvent = queue.removeItemAt(0) as DeltaLoadEvent;
				dispatcher.dispatchEvent(event);		
			}
		}
		
	}
}