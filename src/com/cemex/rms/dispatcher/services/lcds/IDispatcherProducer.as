package com.cemex.rms.dispatcher.services.lcds
{
	public interface IDispatcherProducer
	{
		function sendString(mensaje:String):void;
		function sendObject(mensaje:Object):void;
		function reconnect():void;
	}
}