package com.cemex.rms.dispatcher.services.lcds.events
{
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.FlashIslandsData;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	public class PlantOperativeModeLCDSEvent extends Event
	{
		public static const LCDS_DATA_RECEIVED:String = "plantOperativeModeLCDSDataReceived";
		
		
		public var plantId:String;
		public var zoperativeMode:String;
		public var statusCode:String;
		
		public function PlantOperativeModeLCDSEvent(plantId:String, zoperativeMode:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(LCDS_DATA_RECEIVED, bubbles, cancelable);
			this.plantId = plantId;
			this.zoperativeMode = zoperativeMode;
		}
	}
}