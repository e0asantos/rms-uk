package com.cemex.rms.dispatcher.services.lcds.events
{
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.FlashIslandsData;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	public class VehicleLCDSEvent extends Event
	{
		public static const LCDS_DATA_RECEIVED:String = "vehicleLCDSDataReceived";
		
		public var vehicle:Equipment;
		
		public function VehicleLCDSEvent(vehicle:Equipment, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(LCDS_DATA_RECEIVED, bubbles, cancelable);
			this.vehicle = vehicle;
		}
	}
}