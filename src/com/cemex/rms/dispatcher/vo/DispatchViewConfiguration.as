package com.cemex.rms.dispatcher.vo
{
	public class DispatchViewConfiguration
	{
		public var showOnePlantAtATime:Boolean=false;
		public var availableVehiclesButton:Number=0;
		public var sortVehiclesPerTime:int=0;
		public var rangeVisible:Number=1;
		public var selectedView:String;
		public var sortVehicleFromOldestToNewest:Boolean=true;
		
		public var firstRow:String="ASSG";
		public var secondRow:String="AVLB";
		public var thirdRow:String="LOAD";
		public var plantConfigured:String=null;
		public var windowOpened:Boolean=false;
		public function DispatchViewConfiguration()
		{
		}
	}
}