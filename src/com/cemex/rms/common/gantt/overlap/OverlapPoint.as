package com.cemex.rms.common.gantt.overlap
{
	public class OverlapPoint
	{
		public function OverlapPoint()
		{
		}
		public var index:int = -1;
		public var gradientRatio:int;
		public var fillAlphas:Number;
		public var fillColors:Number;
		public var colorsConfiguration:int;
		public var overlapType:String;
		
		public function toString():String{
			return ""+gradientRatio;
		}

	}
}