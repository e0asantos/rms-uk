package com.cemex.rms.common.gantt
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.tree.AbstractTree;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.views.tree.OrderLoadGanttTask;
	
	import flash.utils.getQualifiedClassName;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;

	public class GanttTree extends AbstractTree
	{
		public function GanttTree(fields:ArrayCollection)
		{
			super(fields);
		}
	
		
		protected function hasSameValues(target:Object,temp:Object):Boolean {
			return true;
		}
		private static var logger:ILogger = LoggerFactory.getLogger("GanttTree");
		
		protected override function compareFunctions(target:Object,temp:Object):Boolean {
			var result:Boolean = false;
			
			if (target is ProductionData){
				//Alert.show("target is '" +target+"'");
			}
			if ((target is GanttTask) && (temp is GanttTask)){
				var targetTask:GanttTask = target as GanttTask;
				var tempTask:GanttTask = temp as GanttTask;
				
				
				// Estamos hablando de la misma carga 
				if (hasSameValues(target,temp)){
					
					if (targetTask.step == GanttTask.STEP_INITIAL || targetTask.step == GanttTask.STEP_MOVED){
						
						result = 
							tempTask.step != GanttTask.STEP_GHOSTX || 
							tempTask.step != GanttTask.STEP_INITIAL || 
							tempTask.step != GanttTask.STEP_MOVED ;
					}
					else if (targetTask.step == GanttTask.STEP_GHOSTX){
						
						result = tempTask.step != GanttTask.STEP_MOVED;
						
					}
					logger.debug("Entro GantTAsk: target:" + getQualifiedClassName(target) + "--temp:"+getQualifiedClassName(temp));
					result = true;
				}
			}
			else if ((target is ProductionData) && (temp is GanttTask)){
				var targetPD:ProductionData = target as ProductionData;
				var orderTask:GanttTask = temp as GanttTask;
				
				
				result = Number(targetPD.vbeln) == Number(orderTask.data.orderNumber)
					&& Number(targetPD.posnr) == Number(orderTask.data.itemNumber);
				logger.debug("Entro Production: target:" + getQualifiedClassName(target) + "--temp:"+getQualifiedClassName(temp));
				//Alert.show("result:"+result + ReflectionHelper.object2XML(targetPD,"targetPD") );
				//Alert.show("result:"+result + ReflectionHelper.object2XML(orderTask,"orderTask") );
				
			}
			else {
				logger.debug("Entro aqui: target:" + getQualifiedClassName(target) + "--temp:"+getQualifiedClassName(temp));
				result = ReflectionHelper.compareObject(target,temp);
			}
			return result;
		}
	}
}