package com.cemex.rms.common.tree
{
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.mediators.PlanningScreenTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.planningScreen.renderers.PlanningScreenTaskRenderer;
	import com.cemex.rms.dispatcher.views.planningScreen.tree.PlanningScreenTask;
	
	import flash.events.Event;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	
	import mx.collections.ArrayCollection;
	import mx.collections.HierarchicalData;
	
	public class ResourceHierarchicalData extends HierarchicalData
	{
		
	
		public function ResourceHierarchicalData(value:Object=null)
		{
			super(value);
		}
		
		
		public override function getChildren(node:Object):Object
		{
			if (node == null)
				return null;
			var children:*;
			if (node is NodeResource)
			{
				var nodo:NodeResource = node as NodeResource;
				return nodo.getChildren();
			}
			//no children exist for this node
			if(children == undefined)
				return null;
			return children;
		}
		
		
		
		public override function canHaveChildren(node:Object):Boolean {
			try {
				if (node is NodeResource) {
					
					var nodo:NodeResource = node as NodeResource;
					
					//return !nodo.hasVisibleTasks;
					if (nodo.children != null && nodo.children.length > 0)  {
						//return !nodo.hasVisibleTasks;	
						return nodo.children.getItemAt(0) is NodeResource;
					}
				}
			}
			catch (e:Error) {
				trace("[Descriptor] exception checking for isBranch (canHaveChildren)");
			}
			return false;
		}
		
		
		public override function hasChildren(node:Object):Boolean {
			try {
/*				var _dvm:DispatcherViewMediator = DispatcherViewMediator.currentInstance;
				if (DispatcherViewMediator.textToSearch!="")
					_dvm.dispatchSearchAux();*/
				if (node is NodeResource) {
					
					var nodo:NodeResource = node as NodeResource;
					if (nodo != null && nodo.getChildren() != null && nodo.getChildren().length > 0)  {
						//return !nodo.hasVisibleTasks;
						return nodo.children.getItemAt(0) is NodeResource;
						//return true;
					}
				}
			}
			catch (e:Error) {
				trace("[Descriptor] exception checking for isBranch (hasChildren)");
			}
			return false;
		}


		

	}
}