package com.cemex.rms.common.tree
{
	public class GenericNode
	{
		public function GenericNode() {
			_data = new Object();
		}
		
		
		
		private var _parent:GenericNode; 
		
		public function getParent():GenericNode{
			return _parent;
		}
		public function setParent(_parent:GenericNode):void{
			this._parent = _parent;
		}
		
		
		protected var _data:Object; 
		public function get data():Object{
			return _data;
		}
		public function set data(_data:Object):void{
			this._data = _data;
			if (_data != null){
				initExtraValues();
			}
		}
		
		public function initExtraValues():void {
			
		}
	}
}