package
{
   import mx.modules.Module;
   import sap.IFlashIsland;
   import sap.IFlashIslandDSName;
   import sap.IFlashIslandDSNameGeneric;
   import sap.IFlashIslandDragAndDrop;
   import sap.IFlashIslandGeneric;
   import sap.IFlashIslandLocale;
   import sap.IFlashIslandTheme;
   import mx.core.mx_internal;
   import mx.core.UIComponent;
   import flash.events.Event;
   import mx.styles.CSSStyleDeclaration;
   import mx.styles.StyleManager;
   import sap.FlashIslandImpl;
   import mx.core.UIComponentDescriptor;
   
   public class FlashIslandModule extends Module implements IFlashIsland, IFlashIslandDSName, IFlashIslandDSNameGeneric, IFlashIslandDragAndDrop, IFlashIslandGeneric, IFlashIslandLocale, IFlashIslandTheme
   {
      
      mx_internal  static var _FlashIslandModule_StylesInit_done:Boolean = false;
      
      
      
      private var islandImpl:FlashIslandImpl;
      
      private var _documentDescriptor_:UIComponentDescriptor;
      
      public function FlashIslandModule()
      {
         _documentDescriptor_ = new UIComponentDescriptor({"type":Module});
         islandImpl = new FlashIslandImpl();
         super();
         mx_internal::_document = this;
         mx_internal::_FlashIslandModule_StylesInit();
         this.layout = "absolute";
      }
      
      public function getLocaleString() : String
      {
         return islandImpl.getLocaleString();
      }
      
      public function fireEvent(param1:UIComponent, param2:String, param3:Object = null) : void
      {
         islandImpl.log.log("[FlashIslandModule.fireEvent]");
         islandImpl.fireEvent(param1,param2,param3);
      }
      
      override public function willTrigger(param1:String) : Boolean
      {
         islandImpl.log.log("[FlashIslandModule.willTrigger]");
         return islandImpl.willTrigger(param1);
      }
      
      public function getDataSourceFieldName(param1:UIComponent, param2:Object) : String
      {
         islandImpl.log.log("[FlashIslandModule.getDataSourceFieldName]");
         return islandImpl.getDataSourceFieldName(param1,param2);
      }
      
      override public function initialize() : void
      {
         mx_internal::setDocumentDescriptor(_documentDescriptor_);
         super.initialize();
      }
      
      public function isSelectedObject(param1:Object, param2:Object, param3:Object) : Boolean
      {
         islandImpl.log.log("[FlashIslandModule.isSelectedObject]");
         return islandImpl.isSelectedObject(param1,param2,param3);
      }
      
      public function setSelected(param1:UIComponent, param2:Object, param3:Object, param4:Boolean) : void
      {
         islandImpl.log.log("[FlashIslandModule.setSelected]");
         islandImpl.setSelected(param1,param2,param3,param4);
      }
      
      public function formatNumber(param1:Number) : String
      {
         return islandImpl.formatNumber(param1);
      }
      
      public function startExternalDrag(param1:Object, param2:Object) : void
      {
         islandImpl.log.log("[FlashIslandModule.startDrag]");
         islandImpl.startExternalDrag(param1,param2);
      }
      
      public function getDecimalSeparator() : String
      {
         return islandImpl.getDecimalSeparator();
      }
      
      public function register(param1:UIComponent) : void
      {
         islandImpl.log.log("[FlashIslandModule.register]");
         islandImpl.register(param1);
      }
      
      public function getDateFormatString() : String
      {
         return islandImpl.getDateFormatString();
      }
      
      public function setLeadSelectionObject(param1:Object, param2:Object, param3:Object) : void
      {
         islandImpl.log.log("[FlashIslandModule.setLeadSelectionObject]");
         islandImpl.setLeadSelectionObject(param1,param2,param3);
      }
      
      public function setSelectedObject(param1:Object, param2:Object, param3:Object, param4:Boolean) : void
      {
         islandImpl.log.log("[FlashIslandModule.setSelectedObject]");
         islandImpl.setSelectedObject(param1,param2,param3,param4);
      }
      
      public function registerDropCompleteHandler(param1:Object, param2:Function) : void
      {
         islandImpl.log.log("[FlashIslandModule.registerDropCompleteHandler]");
         islandImpl.registerDropCompleteHandler(param1,param2);
      }
      
      override public function dispatchEvent(param1:Event) : Boolean
      {
         islandImpl.log.log("[FlashIslandModule.dispatchEvent]");
         return islandImpl.dispatchEvent(param1);
      }
      
      public function storePropertyObject(param1:Object, param2:String, param3:*) : void
      {
         islandImpl.log.log("[FlashIslandModule.storePropertyObject]");
         islandImpl.storePropertyObject(param1,param2,param3);
      }
      
      public function formatDate(param1:Date) : String
      {
         return islandImpl.formatDate(param1);
      }
      
      public function getLeadSelectionObject(param1:Object, param2:Object) : Object
      {
         islandImpl.log.log("[FlashIslandModule.getLeadSelectionObject]");
         return islandImpl.getLeadSelectionObject(param1,param2);
      }
      
      public function registerObject(param1:Object) : void
      {
         islandImpl.log.log("[FlashIslandModule.registerObject]");
         islandImpl.registerObject(param1);
      }
      
      override public function removeEventListener(param1:String, param2:Function, param3:Boolean = false) : void
      {
         islandImpl.log.log("[FlashIslandModule.removeEventListener]");
         islandImpl.removeEventListener(param1,param2,param3);
      }
      
      override public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void
      {
         islandImpl.log.log("[FlashIslandModule.addEventListener]");
         islandImpl.addEventListener(param1,param2,param3,param4,param5);
      }
      
      public function setLeadSelection(param1:UIComponent, param2:Object, param3:Object) : void
      {
         islandImpl.log.log("[FlashIslandModule.setLeadSelection]");
         islandImpl.setLeadSelection(param1,param2,param3);
      }
      
      mx_internal function _FlashIslandModule_StylesInit() : void
      {
         var _loc1_:CSSStyleDeclaration = null;
         var _loc2_:Array = null;
         if(mx_internal::_FlashIslandModule_StylesInit_done)
         {
            return;
         }
         mx_internal::_FlashIslandModule_StylesInit_done = true;
         StyleManager.mx_internal::initProtoChainRoots();
      }
      
      public function setThemeSettings(param1:Object) : void
      {
         islandImpl.log.log("[FlashIslandModule.setThemeSettings]");
         islandImpl.setThemeSettings(param1);
      }
      
      public function storeProperty(param1:UIComponent, param2:String, param3:*) : void
      {
         islandImpl.log.log("[FlashIslandModule.storeProperty]");
         islandImpl.storeProperty(param1,param2,param3);
      }
      
      public function registerDragOverHandler(param1:Object, param2:Function) : void
      {
         islandImpl.log.log("[FlashIslandModule.registerDragOverHandler]");
         islandImpl.registerDragOverHandler(param1,param2);
      }
      
      public function getLeadSelection(param1:UIComponent, param2:Object) : Object
      {
         islandImpl.log.log("[FlashIslandModule.getLeadSelection]");
         return islandImpl.getLeadSelection(param1,param2);
      }
      
      public function isSelected(param1:UIComponent, param2:Object, param3:Object) : Boolean
      {
         islandImpl.log.log("[FlashIslandModule.isSelected]");
         return islandImpl.isSelected(param1,param2,param3);
      }
      
      public function getDataSourceFieldNameObject(param1:Object, param2:Object) : String
      {
         islandImpl.log.log("[FlashIslandModule.getDataSourceFieldNameObject]");
         return islandImpl.getDataSourceFieldNameObject(param1,param2);
      }
      
      public function fireEventObject(param1:Object, param2:String, param3:Object = null) : void
      {
         islandImpl.log.log("[FlashIslandModule.fireEventObject]");
         islandImpl.fireEventObject(param1,param2,param3);
      }
      
      public function getGroupingSeparator() : String
      {
         return islandImpl.getGroupingSeparator();
      }
      
      override public function hasEventListener(param1:String) : Boolean
      {
         islandImpl.log.log("[FlashIslandModule.hasEventListener]");
         return islandImpl.hasEventListener(param1);
      }
   }
}
