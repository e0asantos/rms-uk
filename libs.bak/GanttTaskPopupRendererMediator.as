package com.cemex.rms.dispatcher.gantt.mediators.renderers
{
	import com.cemex.rms.dispatcher.gantt.views.renderers.GanttTaskPopupRenderer;
	import com.cemex.rms.dispatcher.gantt.views.renderers.GanttTaskRenderer;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.requests.ChangePlantRequest;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.utils.getQualifiedClassName;
	
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	import mx.managers.SystemManager;
	import mx.managers.ToolTipManager;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class GanttTaskPopupRendererMediator extends Mediator
	{
		public function GanttTaskPopupRendererMediator()
		{
			super();
		}
		
		[Inject]
		public var view: GanttTaskPopupRenderer;

		[Inject]
		public var ganttService:IGanttServiceFactory;
		
		
		override public function onRegister():void {
			
			view.addEventListener(MouseEvent.CLICK,closeSelected);
			//view.addEventListener(MouseEvent.MOUSE_OVER
			
			view.datagrid.addEventListener(MouseEvent.MOUSE_OUT,closeFocus);
			
		}
		
		public function closeAllWindows():void {
			var popup:GanttTaskPopupRenderer;
			var popupPackage:Array;
			
			
			var systemManager:SystemManager = Application.application.systemManager;
			if (systemManager != null && systemManager.popUpChildren  != null){
				for (var i:int = 0 ; i < systemManager.popUpChildren.numChildren ; i++ ) {
					
					if (systemManager.popUpChildren.getChildAt(i) is GanttTaskPopupRenderer){
						popup = systemManager.popUpChildren.getChildAt(i) as GanttTaskPopupRenderer;
						PopUpManager.removePopUp(popup);
					}
				}
			}
			
		} 
		
		
		
		
		private function closeFocus(event:MouseEvent):void{
		
			var s:String ="" +event.relatedObject;
			if (s.indexOf(""+view)){
				PopUpManager.removePopUp(view);
			}
				
		}
		private function closeSelected(event:MouseEvent):void{
			PopUpManager.removePopUp(view);
		}
	}
}